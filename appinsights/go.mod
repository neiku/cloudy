module bitbucket.org/neiku/cloudy/appinsights

go 1.16

require (
	bitbucket.org/ai69/getmyip v0.0.9
	github.com/microsoft/ApplicationInsights-Go v0.4.4
)

package cloudy

import (
	"sync"
	"time"

	gip "bitbucket.org/ai69/getmyip"
	ais "github.com/microsoft/ApplicationInsights-Go/appinsights"
)

var (
	flushTime = 5 * time.Second
)

// Client represents a wrapper of telemetry client for Azure Application Insights.
type Client struct {
	mu sync.Mutex
	tc ais.TelemetryClient
	wg sync.WaitGroup
}

// NewClient returns a wrapper client for Azure Application Insights.
func NewClient(insKey, endpoint string, cfgMaps ...map[string]string) *Client {
	var url string
	switch endpoint {
	case "@global", "":
		url = "https://dc.services.visualstudio.com/v2/track"
	case "@mooncake":
		url = "https://chinaeast2-0.in.applicationinsights.azure.cn/v2/track"
	default:
		url = endpoint
	}

	cfg := ais.TelemetryConfiguration{
		InstrumentationKey: insKey,
		EndpointUrl:        url,
		MaxBatchSize:       1024,
		MaxBatchInterval:   flushTime,
	}
	cli := ais.NewTelemetryClientFromConfig(&cfg)

	for _, cfgMap := range cfgMaps {
		if len(cfgMap) > 0 {
			tags := cli.Context().Tags
			if v, ok := cfgMap["Version"]; ok {
				tags.Application().SetVer(v)
			}
			if v, ok := cfgMap["Role"]; ok {
				tags.Cloud().SetRole(v)
			}
			if v, ok := cfgMap["Model"]; ok {
				tags.Device().SetModel(v)
			}
		}
	}

	if ip, err := gip.GetPublicIP(); err == nil {
		cli.Context().CommonProperties["PublicIP"] = ip.String()
	}
	if ip, err := gip.GetOutboundIP(); err == nil {
		cli.Context().CommonProperties["InternalIP"] = ip.String()
	}

	return &Client{tc: cli}
}

// SetEnabled enables or disables the telemetry client.
func (t *Client) SetEnabled(enabled bool) {
	t.tc.SetIsEnabled(enabled)
}

// Enabled indicates if the client is enabled and will accept telemetry.
func (t *Client) Enabled() bool {
	return t.tc.IsEnabled()
}

// Origin returns the original client.
func (t *Client) Origin() ais.TelemetryClient {
	return t.tc
}

// Track submits the specified telemetry item.
func (t *Client) Track(tel ais.Telemetry) {
	t.mu.Lock()
	t.wg.Add(1)
	defer t.mu.Unlock()

	go func() {
		t.tc.Track(tel)
		t.wg.Done()
	}()
}

// TrackPanic is a helper method for recovery and submit exceptions.
func (t *Client) TrackPanic(rethrow bool) {
	if r := recover(); r != nil {
		t.tc.TrackException(r)
		t.Flush()
		if rethrow {
			panic(r)
		}
	}
}

// Flush forces the current queue of telemetry client to be sent. It's non-blocking, and not recommended.
func (t *Client) Flush() {
	t.mu.Lock()
	defer t.mu.Unlock()
	t.wg.Wait()
	t.tc.Channel().Flush()
}

// Close flushes and tears down the current queue of telemetry client.
func (t *Client) Close() {
	select {
	case <-t.tc.Channel().Close(flushTime):
	case <-time.After(flushTime * 3): // three times longer
	}
}

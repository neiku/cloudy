package mdb

import (
	"context"
	"log"
	"strings"

	"github.com/qiniu/qmgo"
	"github.com/qiniu/qmgo/options"
)

// Database represents a connection to database.
type Database struct {
	cli *qmgo.Client
	db  *qmgo.Database
}

// New returns an instance as a client to database service.
func New(serverURI, dbName string) *Database {
	ctx := context.Background()
	client, err := qmgo.NewClient(ctx, &qmgo.Config{Uri: serverURI})
	if err != nil {
		log.Printf("fail to create mongodb client: %v", err)
		return nil
	}
	return &Database{cli: client, db: client.Database(dbName)}
}

// Close closes sockets to database service referenced by this instance.
func (d *Database) Close() {
	if err := d.cli.Close(context.Background()); err != nil {
		log.Printf("fail to close mongodb client: %v", err)
	}
}

// Init initializes the database by creating collection and indexes.
// e.g. key -> [index1, index2, ...] --- key is the name of collection,
// ! in index stands for unique index, | for compound index.
func (d *Database) Init(collIndexes map[string][]string) {
	for colName, indexKeys := range collIndexes {
		var opts []options.IndexModel
		for _, key := range indexKeys {
			uniq := strings.HasPrefix(key, "!")
			key = strings.TrimLeft(key, "!")
			opts = append(opts, options.IndexModel{
				Key:    strings.Split(key, "|"),
				Unique: uniq,
			})
		}
		col := d.db.Collection(colName)
		if err := col.CreateIndexes(context.Background(), opts); err != nil {
			log.Printf("fail to create indexes for %q: %v", colName, err)
		}
	}
}

// Collection returns a collection of database by name.
func (d *Database) Collection(name string) *qmgo.Collection {
	return d.db.Collection(name)
}

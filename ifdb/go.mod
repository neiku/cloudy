module bitbucket.org/neiku/cloudy/ifdb

go 1.16

require (
	github.com/influxdata/influxdb-client-go/v2 v2.5.1
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

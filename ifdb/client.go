package ifdb

import (
	"context"
	"strings"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	infapi "github.com/influxdata/influxdb-client-go/v2/api"
	"github.com/influxdata/influxdb-client-go/v2/api/query"
)

type TelegramClient struct {
	orgName    string
	bucketName string
	client     influxdb2.Client
	writer     infapi.WriteAPI
	querier    infapi.QueryAPI
}

func NewTelegramClient(endpointURL, accessToken, orgName, bucketName string) *TelegramClient {
	tc := TelegramClient{
		orgName:    orgName,
		bucketName: bucketName,
		client:     influxdb2.NewClient(endpointURL, accessToken),
	}
	tc.writer = tc.client.WriteAPI(orgName, bucketName)
	tc.querier = tc.client.QueryAPI(orgName)
	return &tc
}

// EnsureBucket creates the named bucket if not found
func (c *TelegramClient) EnsureBucket() error {
	org, err := c.client.OrganizationsAPI().FindOrganizationByName(context.Background(), c.orgName)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			org, err = c.client.OrganizationsAPI().CreateOrganizationWithName(context.Background(), c.orgName)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}

	_, err = c.client.BucketsAPI().FindBucketByName(context.Background(), c.bucketName)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			_, err = c.client.BucketsAPI().CreateBucketWithName(context.Background(), org, c.bucketName)
			if err != nil {
				return nil
			}
		} else {
			return err
		}
	}

	return nil
}

func (c *TelegramClient) Close() {
	c.writer.Flush()
	c.client.Close()
}

func (c *TelegramClient) Report(ts time.Time, measurement string, tags map[string]string, fields map[string]interface{}) {
	p := influxdb2.NewPoint(measurement, tags, fields, ts)
	c.writer.WritePoint(p)
	go func() {
		c.writer.Flush()
	}()
}

func (c *TelegramClient) ReportNow(measurement string, tags map[string]string, fields map[string]interface{}) {
	c.Report(time.Now(), measurement, tags, fields)
}

func (c *TelegramClient) Query(q string) ([]*query.FluxRecord, error) {
	var records []*query.FluxRecord

	result, err := c.querier.Query(context.Background(), q)
	if err != nil {
		return nil, err
	}

	// check for an error
	err = result.Err()
	if err != nil {
		return nil, err
	}

	// iterate over query response
	for result.Next() {
		// Access data
		records = append(records, result.Record())
	}

	return records, nil
}

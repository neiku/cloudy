package mio

import "sync"

// UploadProgress helps accumulate the progress of MinIO file upload
type UploadProgress struct {
	sync.Mutex
	currentBytes int64
	reporter     ProgressReporter
}

// NewUploadProgress creates an instance for a minio upload progress fetcher
func NewUploadProgress(reporter ProgressReporter) *UploadProgress {
	return &UploadProgress{reporter: reporter}
}

func (p *UploadProgress) Read(b []byte) (int, error) {
	n := len(b)
	p.Lock()
	p.currentBytes += int64(n)
	p.reporter(p.currentBytes)
	p.Unlock()

	return n, nil
}

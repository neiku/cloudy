package mio

import (
	"context"
	"fmt"

	"bitbucket.org/neiku/cloudy/util"
	"github.com/1set/gut/ystring"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

// ProgressReporter defines the signature of a callback function invoked as progress is reported.
type ProgressReporter func(bytesTransferred int64)

// Service is a wrapper for Minio client.
type Service struct {
	url  string
	name string
	cli  *minio.Client
}

// Bucket is a wrapper for Minio bucket.
type Bucket struct {
	name string
	svc  *Service
}

// Object is a wrapper for Minio object.
type Object struct {
	key        string
	obj        minio.ObjectInfo
	bkt        *Bucket
	reportProg ProgressReporter
	putOptions minio.PutObjectOptions
}

// NewService creates an instance for a minio client connected to a given minio server.
func NewService(url, id, secret string) (*Service, error) {
	cli, err := minio.New(url, &minio.Options{
		Creds:  credentials.NewStaticV4(id, secret, ""),
		Secure: false,
	})
	if err != nil {
		return nil, fmt.Errorf(`mio: %w`, err)
	}
	return &Service{
		url:  url,
		name: fmt.Sprintf(`%s@%s`, id, url),
		cli:  cli,
	}, nil
}

// Name returns the name of the service.
func (s *Service) Name() string {
	return s.name
}

func (s Service) String() string {
	return fmt.Sprintf(`S{%s}`, s.Name())
}

// ListBucket lists all buckets under the service.
func (s *Service) ListBucket() ([]*Bucket, error) {
	var buckets []*Bucket
	bs, err := s.cli.ListBuckets(context.Background())
	if err != nil {
		return nil, fmt.Errorf(`mio: %w`, err)
	}
	for _, b := range bs {
		buckets = append(buckets, &Bucket{
			name: b.Name,
			svc:  s,
		})
	}
	return buckets, nil
}

// NewBucket creates an instance for the bucket with given name.
func (s *Service) NewBucket(name string) (*Bucket, error) {
	if ystring.IsEmpty(name) {
		return nil, fmt.Errorf(`mio: empty bucket name`)
	}
	return &Bucket{
		name: name,
		svc:  s,
	}, nil
}

func (b *Bucket) String() string {
	return fmt.Sprintf(`B{%s➟%s}`, b.svc.url, b.name)
}

// Create creates the bucket with preset name if it doesn't exist.
func (b *Bucket) Create() error {
	ctx := context.Background()
	if err := b.svc.cli.MakeBucket(ctx, b.name, minio.MakeBucketOptions{}); err != nil {
		// check to see if we already own this bucket
		exists, errBucketExists := b.svc.cli.BucketExists(ctx, b.name)
		if exists && errBucketExists == nil {
			return nil
		}
		return fmt.Errorf(`mio: %w`, err)
	}
	return nil
}

// Destroy deletes the bucket if it exists.
func (b *Bucket) Destroy() error {
	err := b.svc.cli.RemoveBucket(context.Background(), b.name)
	if err != nil {
		return fmt.Errorf(`mio: %w`, err)
	}
	return nil
}

// ListObject lists all objects in the bucket.
func (b *Bucket) ListObject() []*Object {
	var objs []*Object
	objCh := b.svc.cli.ListObjects(context.Background(), b.name, minio.ListObjectsOptions{})
	for obj := range objCh {
		objs = append(objs, &Object{
			key: obj.Key,
			obj: obj,
			bkt: b,
		})
	}
	return objs
}

// NewObject creates an instance for the object with given name.
func (b *Bucket) NewObject(name string) (*Object, error) {
	if ystring.IsEmpty(name) {
		return nil, fmt.Errorf(`mio: empty object name`)
	}
	return &Object{
		key:        name,
		bkt:        b,
		putOptions: minio.PutObjectOptions{},
	}, nil
}

func (o *Object) String() string {
	return fmt.Sprintf(`O{%s (%v)}`, o.key, util.HumanizeIBytes(uint64(o.obj.Size)))
}

// SetProgressReporter sets ProgressReporter of the object for downloading and uploading.
func (o *Object) SetProgressReporter(report ProgressReporter) {
	o.reportProg = report
}

// SetContentType sets ContentType of the object.
func (o *Object) SetContentType(contentType string) {
	o.putOptions.ContentType = contentType
}

// SetDisableMultipart sets DisableMultipart of the object for uploading.
func (o *Object) SetDisableMultipart(disableMultipart bool) {
	o.putOptions.DisableMultipart = disableMultipart
}

// Exist checks if the object exists, or return an error if it doesn't.
func (o *Object) Exist() error {
	ctx := context.Background()
	// get the object info
	obj, err := o.bkt.svc.cli.StatObject(ctx, o.bkt.name, o.key, minio.StatObjectOptions{})
	if err != nil {
		return fmt.Errorf(`mio: %w`, err)
	}
	o.obj = obj
	return nil
}

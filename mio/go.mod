module bitbucket.org/neiku/cloudy/mio

go 1.16

require (
	bitbucket.org/neiku/cloudy/util v0.0.1
	bitbucket.org/neiku/hlog v0.1.2
	github.com/1set/gut v0.0.0-20201117175203-a82363231997
	github.com/Azure/azure-pipeline-go v0.2.3
	github.com/BurntSushi/toml v1.2.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/minio/minio-go/v7 v7.0.15
	go.uber.org/zap v1.23.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
)

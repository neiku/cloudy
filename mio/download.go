package mio

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"strings"

	"bitbucket.org/neiku/cloudy/util"
	yh "github.com/1set/gut/yhash"
	yo "github.com/1set/gut/yos"
	azp "github.com/Azure/azure-pipeline-go/pipeline"
	"github.com/minio/minio-go/v7"
	"go.uber.org/zap"
)

// DownloadContent downloads the content of the object and returns in a slice of bytes.
func (o *Object) DownloadContent(ctx context.Context) ([]byte, error) {
	data := bytes.Buffer{}
	err := o.DownloadStream(func(stream io.ReadCloser) error {
		return copyStream(ctx, bufio.NewWriter(&data), stream)
	})

	if err != nil {
		return nil, fmt.Errorf(`mio: %w`, err)
	}
	return data.Bytes(), nil
}

// DownloadFile downloads the object and saves as a file in the given path.
func (o *Object) DownloadFile(ctx context.Context, fileName string) error {
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer func() { _ = file.Close() }()

	err = o.DownloadStream(func(stream io.ReadCloser) error {
		return copyStream(ctx, file, stream)
	})
	return err
}

// DownloadFileIfNewer downloads the object and saves as a file in the given path
// if the file doesn't exist or has a different hash.
func (o *Object) DownloadFileIfNewer(ctx context.Context, fileName string) error {
	if yo.NotExist(fileName) {
		return o.DownloadFile(ctx, fileName)
	}

	fileHash, err := yh.FileMD5(fileName)
	if err != nil {
		return err
	}

	obj, err := o.bkt.svc.cli.GetObject(ctx, o.bkt.name, o.key, minio.GetObjectOptions{})
	if err != nil {
		return fmt.Errorf(`mio: %w`, err)
	}

	objInfo, err := obj.Stat()
	if err != nil {
		return fmt.Errorf(`mio: %w`, err)
	}

	blobMD5, ok := objInfo.UserMetadata["Md5"]

	l := log.With(
		zap.String("target", o.String()),
		zap.String("content_type", objInfo.ContentType),
		zap.Int64("content_len", objInfo.Size),
		zap.String("file_hash_md5", fileHash),
		zap.String("blob_hash_md5", blobMD5),
		zap.Time("last_modified", objInfo.LastModified),
	)
	l.Infow("get obj info", "user metadata", objInfo.UserMetadata)
	if ok && strings.EqualFold(fileHash, blobMD5) {
		l.Debugw("skip downloading blob with same content")
		return nil
	} else {
		l.Debugw("check local blob has different content")
		return o.DownloadFile(ctx, fileName)
	}
}

func (o *Object) DownloadStream(handler func(io.ReadCloser) error) error {
	ctx := context.Background()
	// get object
	obj, err := o.bkt.svc.cli.GetObject(ctx, o.bkt.name, o.key, minio.GetObjectOptions{})
	if err != nil {
		return fmt.Errorf(`mio: %w`, err)
	}

	objInfo, err := obj.Stat()
	if err != nil {
		return fmt.Errorf(`mio: %w`, err)
	}

	totalBytes := objInfo.Size
	log.Debugw("will download blob",
		zap.String("target", o.String()),
		zap.String("content_type", objInfo.ContentType),
		zap.Int64("content_len", totalBytes),
		zap.String("hash_md5", objInfo.UserMetadata["Md5"]),
		zap.Time("last_modified", objInfo.LastModified),
	)

	var stream io.ReadCloser = obj
	if totalBytes > 1024*1024 {
		var reporter ProgressReporter
		if o.reportProg != nil {
			reporter = o.reportProg
		} else {
			ps := util.NewProgressLogSampler(uint64(totalBytes))
			totalSize := ps.TotalSize()
			reporter = func(bytesTransferred int64) {
				if !ps.Sample(uint64(bytesTransferred)) {
					return
				}
				log.Debugw("download in progress",
					zap.String("target", o.String()),
					zap.String("total_size", totalSize),
					zap.String("received_size", ps.CurrentSize()),
					zap.String("speed", ps.Speed()),
					zap.String("progress", ps.Progress()),
					zap.String("time_spent", ps.TimeCost()),
				)
			}
		}
		stream = azp.NewResponseBodyProgress(stream, azp.ProgressReceiver(reporter))
	}
	defer func() { _ = stream.Close() }()

	return handler(stream)
}

type readerFunc func(p []byte) (n int, err error)

func (rf readerFunc) Read(p []byte) (n int, err error) { return rf(p) }

func copyStream(ctx context.Context, dst io.Writer, src io.Reader) error {
	// Copy will call the Reader and Writer interface multiple time, in order
	// to copy by chunk (avoiding loading the whole file in memory).
	// I insert the ability to cancel before read time as it is the earliest
	// possible in the call process.
	_, err := io.Copy(dst, readerFunc(func(p []byte) (int, error) {
		// golang non-blocking channel: https://gobyexample.com/non-blocking-channel-operations
		select {
		// if context has been canceled
		case <-ctx.Done():
			// stop process and propagate "context canceled" error
			return 0, ctx.Err()
		default:
			// otherwise just run default io.Reader implementation
			return src.Read(p)
		}
	}))
	return err
}

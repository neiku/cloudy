package mio

import (
	"bytes"
	"context"
	"fmt"
	"os"

	"bitbucket.org/neiku/cloudy/util"
	yh "github.com/1set/gut/yhash"
	ys "github.com/1set/gut/ystring"
	"go.uber.org/zap"
)

// UploadContent uploads the given data to the object.
func (o *Object) UploadContent(ctx context.Context, data []byte) error {
	var reporter ProgressReporter
	if o.reportProg != nil {
		reporter = o.reportProg
	} else {
		ps := util.NewProgressLogSampler(uint64(len(data)))
		totalSize := ps.TotalSize()
		log.Debugw("will upload file as blob",
			zap.String("target", o.String()),
			zap.Int64("content_len", int64(len(data))),
			zap.String("total_size", totalSize),
		)
		reporter = func(bytesTransferred int64) {
			if !ps.Sample(uint64(bytesTransferred)) {
				return
			}
			log.Debugw("upload file in progress",
				zap.String("target", o.String()),
				zap.String("total_size", totalSize),
				zap.String("sent_size", ps.CurrentSize()),
				zap.String("speed", ps.Speed()),
				zap.String("progress", ps.Progress()),
				zap.String("time_spent", ps.TimeCost()),
			)
		}
	}

	o.putOptions.Progress = NewUploadProgress(reporter)
	o.putOptions.SendContentMd5 = true
	if o.putOptions.ContentType == "" {
		if contentType := util.InferContentTypeByName(o.key); ys.IsNotEmpty(contentType) {
			o.putOptions.ContentType = contentType
		} else {
			o.putOptions.ContentType = util.InferContentTypeByData(data)
		}
	}

	// upload the data
	info, err := o.bkt.svc.cli.PutObject(ctx, o.bkt.name, o.key, bytes.NewReader(data), int64(len(data)), o.putOptions)
	if err != nil {
		return fmt.Errorf(`mio: %w`, err)
	}

	log.Debugw("uploaded content as blob",
		zap.String("target", o.String()),
		zap.Time("last_modified", info.LastModified),
	)
	// load the object info
	return o.Exist()
}

// UploadFile uploads the given file to the object.
func (o *Object) UploadFile(ctx context.Context, fileName string) error {
	fileMD5, err := yh.FileMD5(fileName)
	if err != nil {
		return err
	}
	return o.UploadFileWithMD5(ctx, fileName, fileMD5)
}

func (o *Object) UploadFileWithMD5(ctx context.Context, fileName string, md5 string) error {
	file, err := os.Open(fileName)
	if err != nil {
		return err
	}
	defer func() { _ = file.Close() }()

	stat, err := file.Stat()
	if err != nil {
		return err
	}

	var reporter ProgressReporter
	if o.reportProg != nil {
		reporter = o.reportProg
	} else {
		ps := util.NewProgressLogSampler(uint64(stat.Size()))
		totalSize := ps.TotalSize()
		log.Debugw("will upload file as blob",
			zap.String("file", stat.Name()),
			zap.String("target", o.String()),
			zap.Int64("content_len", int64(stat.Size())),
			zap.String("total_size", totalSize),
		)
		reporter = func(bytesTransferred int64) {
			if !ps.Sample(uint64(bytesTransferred)) {
				return
			}
			log.Debugw("upload file in progress",
				zap.String("target", o.String()),
				zap.String("total_size", totalSize),
				zap.String("sent_size", ps.CurrentSize()),
				zap.String("speed", ps.Speed()),
				zap.String("progress", ps.Progress()),
				zap.String("time_spent", ps.TimeCost()),
			)
		}
	}

	var userMeta map[string]string
	if ys.IsNotBlank(md5) {
		userMeta = map[string]string{"Md5": md5}
	}

	o.putOptions.UserMetadata = userMeta
	o.putOptions.Progress = NewUploadProgress(reporter)
	if o.putOptions.ContentType == "" {
		o.putOptions.ContentType = util.InferContentTypeByNameAndData(fileName)
	}

	if _, err := o.bkt.svc.cli.FPutObject(ctx, o.bkt.name, o.key, fileName, o.putOptions); err != nil {
		return fmt.Errorf(`mio: %w`, err)
	}
	// load the object info
	return o.Exist()
}

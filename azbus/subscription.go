package azbus

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus"
	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus/admin"
)

// Subscription wraps the Azure Service Bus subscription of a topic.
type Subscription struct {
	ctx       context.Context
	topic     *Topic
	subName   string
	prop      *admin.SubscriptionProperties
	receiveMu sync.Mutex
	receiver  *azservicebus.Receiver
}

func (s *Subscription) String() string {
	r := s.subName
	if p := s.prop; p != nil {
		r += fmt.Sprintf(`[%s/%s/%d]`, *p.LockDuration, *p.DefaultMessageTimeToLive, *p.MaxDeliveryCount)
	}
	return fmt.Sprintf(`📩Subscription{%s}`, r)
}

// NewSubscription returns the subscription with the given name of the topic. It returns error if the subscription does not exist.
func (t *Topic) NewSubscription(subName string) (*Subscription, error) {
	return t.newSubscriptionWithReceiverMode(subName, defaultReceiverMode)
}

// NewSubscriptionWithPeekLockMode returns the subscription with the given name of the topic with PeekLock mode. It returns error if the subscription does not exist.
func (t *Topic) NewSubscriptionWithPeekLockMode(subName string) (*Subscription, error) {
	return t.newSubscriptionWithReceiverMode(subName, receiverModePeekLock)
}

// NewSubscriptionWithReceiveAndDeleteMode returns the subscription with the given name of the topic with ReceiveAndDelete mode. It returns error if the subscription does not exist.
func (t *Topic) NewSubscriptionWithReceiveAndDeleteMode(subName string) (*Subscription, error) {
	return t.newSubscriptionWithReceiverMode(subName, receiverModeRecvNDel)
}

func (t *Topic) newSubscriptionWithReceiverMode(subName string, receiverMode *azservicebus.ReceiverOptions) (*Subscription, error) {
	// get subscription properties
	ts := time.Now()
	sub, err := t.admin.GetSubscription(t.ctx, t.topicName, subName, nil)
	if err != nil {
		return nil, err
	} else if sub == nil {
		return nil, ErrSubscriptionNotFound
	}
	// create receiver
	recv, err := t.client.NewReceiverForSubscription(t.topicName, sub.SubscriptionName, receiverMode)
	if err != nil {
		return nil, err
	}
	// get properties
	subRun, err := t.admin.GetSubscriptionRuntimeProperties(t.ctx, t.topicName, sub.SubscriptionName, nil)
	if err != nil {
		return nil, err
	} else if subRun == nil {
		return nil, ErrSubscriptionNotFound
	}
	st := &Status{
		TotalMessageCount:              subRun.TotalMessageCount,
		ActiveMessageCount:             subRun.ActiveMessageCount,
		DeadLetterMessageCount:         subRun.DeadLetterMessageCount,
		TransferMessageCount:           subRun.TransferMessageCount,
		TransferDeadLetterMessageCount: subRun.TransferDeadLetterMessageCount,
	}
	log.Debugw("message subscription initialized", "topic", t.topicName, "subscription", sub.SubscriptionName, "status", st, "time_elapsed", time.Since(ts))
	// finally return the instance
	return &Subscription{
		topic:    t,
		ctx:      context.Background(),
		subName:  sub.SubscriptionName,
		prop:     &sub.SubscriptionProperties,
		receiver: recv,
	}, nil
}

// createSubscription creates a subscription with the given name and rule. It returns error if the subscription already exists.
func (t *Topic) createSubscription(subName string, rule *admin.RuleProperties, lockDur, msgTTL *string) (*Subscription, error) {
	// create subscription
	opt := &admin.CreateSubscriptionOptions{
		Properties: &admin.SubscriptionProperties{
			DefaultRule:              rule,
			LockDuration:             lockDur,
			DefaultMessageTimeToLive: msgTTL,
		},
	}
	res, err := t.admin.CreateSubscription(t.ctx, t.topicName, subName, opt)
	if err != nil {
		if strings.Contains(err.Error(), respErr409) {
			return nil, ErrConflict
		}
		return nil, err
	}
	log.Debugw("subscription created", "topic", t.topicName, "subscription", res.SubscriptionName, "status", res.Status, "lock", res.LockDuration, "ttl", res.DefaultMessageTimeToLive)
	// return subscription instance
	return t.NewSubscription(res.SubscriptionName)
}

// deleteSubscription deletes the subscription with the given name.
func (t *Topic) deleteSubscription(subName string) error {
	ts := time.Now()
	_, err := t.admin.DeleteSubscription(t.ctx, t.topicName, subName, nil)
	if err != nil {
		if err.Error() == respErr404 {
			return ErrSubscriptionNotFound
		}
		return err
	}
	log.Debugw("subscription deleted", "topic", t.topicName, "subscription", subName, "time_elapsed", time.Since(ts))
	return nil
}

// GetStatus returns the status of the subscription.
func (s *Subscription) GetStatus() (*Status, error) {
	subRun, err := s.topic.admin.GetSubscriptionRuntimeProperties(s.ctx, s.topic.topicName, s.subName, nil)
	if err != nil {
		return nil, err
	} else if subRun == nil {
		return nil, ErrSubscriptionNotFound
	}
	return &Status{
		TotalMessageCount:              subRun.TotalMessageCount,
		ActiveMessageCount:             subRun.ActiveMessageCount,
		DeadLetterMessageCount:         subRun.DeadLetterMessageCount,
		TransferMessageCount:           subRun.TransferMessageCount,
		TransferDeadLetterMessageCount: subRun.TransferDeadLetterMessageCount,
	}, nil
}

// Close closes the receiver of the subscription.
func (s *Subscription) Close() error {
	s.receiveMu.Lock()
	defer s.receiveMu.Unlock()

	if s.receiver != nil {
		return s.receiver.Close(s.ctx)
	}
	return nil
}

// Receive returns at most 10 messages from the queue. It returns no error if no message is available.
// Don't call Receive() concurrently, or would be blocked by the mutex.
func (s *Subscription) Receive() ([]*Message, error) {
	s.receiveMu.Lock()
	defer s.receiveMu.Unlock()

	return receiveMsg(s.receiver)
}

// Take marks a message as taken, so it will not be returned before the visibility timeout.
// It usually used to extend the time to process the message.
func (s *Subscription) Take(m *Message) error {
	if m == nil {
		return ErrInvalidMessage
	}
	lockedUntil := m.msg.LockedUntil
	err := s.receiver.RenewMessageLock(s.ctx, m.msg, nil)
	if err != nil {
		log.Warnw("failed to renew message lock", "message", m.GetID(), "error", err)
		return err
	}
	log.Infow("message lock renewed", "message", m.GetID(), "before", lockedUntil, "after", m.msg.LockedUntil)
	return nil
}

// Abandon marks a message as not taken, so it will be returned immediately.
func (s *Subscription) Abandon(m *Message) error {
	if m == nil {
		return ErrInvalidMessage
	}
	return s.receiver.AbandonMessage(s.ctx, m.msg, nil)
}

// Complete marks a message as completed, so it will be deleted from the queue.
func (s *Subscription) Complete(m *Message) error {
	if m == nil {
		return ErrInvalidMessage
	}
	return s.receiver.CompleteMessage(s.ctx, m.msg, nil)
}

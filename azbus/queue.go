package azbus

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus"
	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus/admin"
)

// Queue represents a instance of Azure Service Bus Queue.
type Queue struct {
	ctx       context.Context
	client    *azservicebus.Client
	admin     *admin.Client
	queueName string
	prop      *admin.QueueProperties
	sender    *azservicebus.Sender
	receiver  *azservicebus.Receiver
	sendMu    sync.Mutex
	receiveMu sync.Mutex
}

func (q *Queue) String() string {
	s := q.queueName
	if p := q.prop; p != nil {
		s += fmt.Sprintf(`[%dM/%s/%d]`, *p.MaxSizeInMegabytes, *p.LockDuration, *p.MaxDeliveryCount)
	}
	return fmt.Sprintf(`📥Queue{%s}`, s)
}

// NewQueue creates a new instance of Queue.
func NewQueue(connectionString, queueName string) (*Queue, error) {
	return newQueueWithReceiverMode(connectionString, queueName, defaultReceiverMode)
}

// NewQueueWithPeekLockMode creates a new instance of Queue with PeekLock mode.
func NewQueueWithPeekLockMode(connectionString, queueName string) (*Queue, error) {
	return newQueueWithReceiverMode(connectionString, queueName, receiverModePeekLock)
}

// NewQueueWithReceiveAndDeleteMode creates a new instance of Queue with ReceiveAndDelete mode.
func NewQueueWithReceiveAndDeleteMode(connectionString, queueName string) (*Queue, error) {
	return newQueueWithReceiverMode(connectionString, queueName, receiverModeRecvNDel)
}

func newQueueWithReceiverMode(connectionString, queueName string, receiverMode *azservicebus.ReceiverOptions) (*Queue, error) {
	// create queue clients
	ts := time.Now()
	ctx := context.Background()
	client, err := azservicebus.NewClientFromConnectionString(connectionString, nil)
	if err != nil {
		return nil, err
	}
	receiver, err := client.NewReceiverForQueue(queueName, receiverMode)
	if err != nil {
		return nil, err
	}
	sender, err := client.NewSender(queueName, nil)
	if err != nil {
		return nil, err
	}
	// create admin client
	adminCli, err := admin.NewClientFromConnectionString(connectionString, nil)
	if err != nil {
		return nil, err
	}
	// get queue properties
	queProp, err := adminCli.GetQueue(ctx, queueName, nil)
	if err != nil {
		return nil, err
	} else if queProp == nil {
		return nil, ErrQueueNotFound
	}
	queRun, err := adminCli.GetQueueRuntimeProperties(ctx, queueName, nil)
	if err != nil {
		return nil, err
	} else if queRun == nil {
		return nil, ErrQueueNotFound
	}
	qs := &Status{
		SizeInBytes:            queRun.SizeInBytes,
		TotalMessageCount:      queRun.TotalMessageCount,
		ActiveMessageCount:     queRun.ActiveMessageCount,
		DeadLetterMessageCount: queRun.DeadLetterMessageCount,
		ScheduledMessageCount:  queRun.ScheduledMessageCount,
	}

	log.Debugw("message queue initialized", "queue", queueName, "status", qs, "time_elapsed", time.Since(ts))
	// finally return the instance
	return &Queue{
		ctx:       ctx,
		client:    client,
		admin:     adminCli,
		prop:      &queProp.QueueProperties,
		queueName: queueName,
		sender:    sender,
		receiver:  receiver,
	}, nil
}

// Close closes the sender and receiver of the queue.
func (q *Queue) Close() error {
	q.sendMu.Lock()
	defer q.sendMu.Unlock()
	if err := q.sender.Close(q.ctx); err != nil {
		return err
	}

	q.receiveMu.Lock()
	defer q.receiveMu.Unlock()
	if err := q.receiver.Close(q.ctx); err != nil {
		return err
	}

	return q.client.Close(q.ctx)
}

// GetStatus returns the status of the queue.
func (q *Queue) GetStatus() (*Status, error) {
	queRun, err := q.admin.GetQueueRuntimeProperties(q.ctx, q.queueName, nil)
	if err != nil {
		return nil, err
	}
	return &Status{
		SizeInBytes:            queRun.SizeInBytes,
		TotalMessageCount:      queRun.TotalMessageCount,
		ActiveMessageCount:     queRun.ActiveMessageCount,
		DeadLetterMessageCount: queRun.DeadLetterMessageCount,
		ScheduledMessageCount:  queRun.ScheduledMessageCount,
	}, nil
}

// Send sends a message to the queue immediately. If attrs is nil and data is empty, an error will be returned.
func (q *Queue) Send(data []byte, attrs Attributes) error {
	q.sendMu.Lock()
	defer q.sendMu.Unlock()

	return sendMsg(q.sender, data, attrs)
}

// Schedule schedules a message to be enqueue at the given time, and returns the sequence number of the message.
func (q *Queue) Schedule(data []byte, attrs Attributes, sched time.Time) (int64, error) {
	q.sendMu.Lock()
	defer q.sendMu.Unlock()

	return scheduleMsg(q.sender, data, attrs, sched)
}

// Receive returns at most 10 messages from the queue. It returns no error if no message is available.
// Don't call Receive() concurrently, or would be blocked by the mutex.
func (q *Queue) Receive() ([]*Message, error) {
	q.receiveMu.Lock()
	defer q.receiveMu.Unlock()

	return receiveMsg(q.receiver)
}

// Take marks a message as taken, so it will not be returned before the visibility timeout.
// It usually used to extend the time to process the message.
func (q *Queue) Take(m *Message) error {
	if m == nil {
		return ErrInvalidMessage
	}
	return q.receiver.RenewMessageLock(q.ctx, m.msg, nil)
}

// Abandon marks a message as not taken, so it will be returned immediately.
func (q *Queue) Abandon(m *Message) error {
	if m == nil {
		return ErrInvalidMessage
	}
	return q.receiver.AbandonMessage(q.ctx, m.msg, nil)
}

// Complete marks a message as completed, so it will be deleted from the queue.
func (q *Queue) Complete(m *Message) error {
	if m == nil {
		return ErrInvalidMessage
	}
	return q.receiver.CompleteMessage(q.ctx, m.msg, nil)
}

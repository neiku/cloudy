package azbus

import (
	"context"
	"errors"
	"time"

	"bitbucket.org/ai69/amoy"
	"github.com/1set/gut/yrand"
	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus"
	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus/admin"
)

var (
	// ErrQueueNotFound is returned when the queue is not found.
	ErrQueueNotFound = errors.New("queue not found")
	// ErrTopicNotFound is returned when the topic is not found.
	ErrTopicNotFound = errors.New("topic not found")
	// ErrSubscriptionNotFound is returned when the subscription is not found.
	ErrSubscriptionNotFound = errors.New("subscription not found")
	// ErrConflict is returned when the resource already exists.
	ErrConflict = errors.New("already exists")
	// ErrInvalidMessage is returned when the message is invalid.
	ErrInvalidMessage = errors.New("invalid message")
	// ErrInvalidSequenceNumber is returned when the sequence number is invalid.
	ErrInvalidSequenceNumber = errors.New("invalid sequence number")
	// ErrInvalidScheduleTime is returned when the schedule time is invalid.
	ErrInvalidScheduleTime = errors.New("invalid schedule time")
)

const (
	respErr404 = `RESPONSE 404: 404 Not Found`
	respErr409 = `RESPONSE 409: 409 Conflict`
)

// Attributes represents the attributes of a message.
type Attributes map[string]interface{}

// Tags represents the tags of a message.
type Tags map[string]interface{}

// Status represents the status of a queue or topic.
type Status struct {
	SizeInBytes                    int64 `json:"size_bytes,omitempty"`
	TotalMessageCount              int64 `json:"total_message,omitempty"`
	ActiveMessageCount             int32 `json:"active_message,omitempty"`
	DeadLetterMessageCount         int32 `json:"dead_letter_message,omitempty"`
	ScheduledMessageCount          int32 `json:"scheduled_message,omitempty"`
	SubscriptionCount              int32 `json:"subscription,omitempty"`
	TransferMessageCount           int32 `json:"transfer_message,omitempty"`
	TransferDeadLetterMessageCount int32 `json:"transfer_dead_letter_message,omitempty"`
}

var (
	msgRecvBatchSize = 1
)

// SetReceiveBatchSize sets the batch size for receiving messages.
func SetReceiveBatchSize(size int) {
	msgRecvBatchSize = size
}

// GetToNameRule returns a rule that filters messages by the given name.
func GetToNameRule(name string) *admin.RuleProperties {
	return &admin.RuleProperties{
		Name: "To_Name",
		Filter: &admin.CorrelationFilter{
			To: amoy.StringPtr(name),
		},
	}
}

// GetFromNameRule returns a rule that filters messages by the given name.
func GetFromNameRule(name string) *admin.RuleProperties {
	return &admin.RuleProperties{
		Name: "From_Name",
		Filter: &admin.CorrelationFilter{
			ReplyTo: amoy.StringPtr(name),
		},
	}
}

// genMsgID generates a random message ID.
func genMsgID() *string {
	const keys = `ABCDEFGHJKLMNPQRSTUVWXYZ23456789`
	raw, err := yrand.String(keys, 8)
	if err != nil {
		return nil
	}
	return amoy.StringPtr(raw)
}

func dataToMsg(data []byte, attrs Attributes) *azservicebus.Message {
	var (
		propMsgID    *string
		propCorelID  *string
		propReplyTo  *string
		propTo       *string
		propSession  *string
		propSubject  *string
		propContType *string
	)
	// extract predefined attributes
	if len(attrs) > 0 {
		for k, v := range attrs {
			s, ok := v.(string)
			if !ok {
				continue
			}
			switch k {
			case `message_id`, "MessageID":
				propMsgID = amoy.StringPtr(s)
			case `correlation_id`, "CorrelationID":
				propCorelID = amoy.StringPtr(s)
			case `reply_to`, "ReplyTo", "from", "From":
				propReplyTo = amoy.StringPtr(s)
			case `to`, "To":
				propTo = amoy.StringPtr(s)
			case `session_id`, "SessionID":
				propSession = amoy.StringPtr(s)
			case `subject`, "Subject":
				propSubject = amoy.StringPtr(s)
			case `content_type`, "ContentType":
				propContType = amoy.StringPtr(s)
			default:
				continue
			}
			// delete the attribute
			delete(attrs, k)
		}
	}
	// create message
	if propMsgID == nil {
		propMsgID = genMsgID()
	}
	msg := &azservicebus.Message{
		Body:                  data,
		ApplicationProperties: attrs,
		MessageID:             propMsgID,
		CorrelationID:         propCorelID,
		ReplyTo:               propReplyTo,
		To:                    propTo,
		SessionID:             propSession,
		Subject:               propSubject,
		ContentType:           propContType,
	}
	return msg
}

func sendMsg(sender *azservicebus.Sender, data []byte, attrs Attributes) error {
	// refuse empty message
	if attrs == nil && len(data) == 0 {
		return ErrInvalidMessage
	}

	// refuse empty message
	if attrs == nil && len(data) == 0 {
		return ErrInvalidMessage
	}

	// send it
	ctx := context.Background()
	msg := dataToMsg(data, attrs)
	err := sender.SendMessage(ctx, msg, nil)
	if err != nil {
		return err
	}
	log.Debugw("message sent", "message_id", msg.MessageID, "data_size", len(msg.Body), "attrs", len(msg.ApplicationProperties))
	return nil
}

func scheduleMsg(sender *azservicebus.Sender, data []byte, attrs Attributes, sched time.Time) (int64, error) {
	// refuse empty message
	if attrs == nil && len(data) == 0 {
		return 0, ErrInvalidMessage
	}

	// schedule time must be in the future
	if sched.Before(time.Now()) {
		return 0, ErrInvalidScheduleTime
	}

	// schedule it
	ctx := context.Background()
	msg := dataToMsg(data, attrs)
	msg.ScheduledEnqueueTime = &sched
	ids, err := sender.ScheduleMessages(ctx, []*azservicebus.Message{msg}, sched, nil)
	if err != nil {
		return 0, err
	}
	if len(ids) == 0 {
		return 0, ErrInvalidSequenceNumber
	}
	log.Debugw("message scheduled", "sequence_number", ids[0], "message_id", msg.MessageID, "data_size", len(msg.Body), "attrs", len(msg.ApplicationProperties), "expect_time", sched)
	return ids[0], nil
}

func receiveMsg(receiver *azservicebus.Receiver) ([]*Message, error) {
	ts := time.Now()
	ctx := context.Background()
	messages, err := receiver.ReceiveMessages(ctx, msgRecvBatchSize, nil)
	if err != nil {
		return nil, err
	}
	if cnt := len(messages); cnt > 0 {
		log.Debugw("batch messages received", "batch", msgRecvBatchSize, "count", cnt, "time_elapsed", time.Since(ts))
	} else {
		log.Debugw("no message received", "time_elapsed", time.Since(ts))
	}

	var batch []*Message
	for _, msg := range messages {
		batch = append(batch, &Message{
			msg: msg,
		})
	}
	return batch, nil
}

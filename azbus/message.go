package azbus

import (
	"fmt"

	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus"
)

// Message represents a message received from Azure Service Bus.
type Message struct {
	msg *azservicebus.ReceivedMessage
	tag Tags
}

// String returns the string representation of the message.
func (m *Message) String() string {
	var s string
	if m != nil && m.msg != nil {
		s = m.msg.MessageID
		if t := m.msg.EnqueuedTime; t != nil {
			s += "@" + t.Format(`01-02T15:04:05.999Z`)
		}
		s += fmt.Sprintf(`|B:%d|A:%d`, len(m.msg.Body), len(m.msg.ApplicationProperties))
	}
	return fmt.Sprintf(`📃Message{%s}`, s)
}

// GetID returns the ID of the message.
func (m *Message) GetID() string {
	var id string
	if m != nil && m.msg != nil {
		id = m.msg.MessageID
	}
	return id
}

// GetData returns the data of the message in bytes.
func (m *Message) GetData() []byte {
	if m != nil && m.msg != nil {
		return m.msg.Body
	}
	return nil
}

// GetContent returns the content of the message in string.
func (m *Message) GetContent() string {
	if m != nil && m.msg != nil {
		return string(m.msg.Body)
	}
	return ""
}

// GetAttributes returns the attributes of the message.
func (m *Message) GetAttributes() Attributes {
	if m != nil && m.msg != nil {
		attrs := Attributes(m.msg.ApplicationProperties)
		return attrs
	}
	return nil
}

// SetTags sets the local tags of the message. It will not be sent to the server, and only used locally.
func (m *Message) SetTags(tag Tags) {
	if m != nil {
		m.tag = tag
	}
}

// GetTags returns the local tags of the message.
func (m *Message) GetTags() Tags {
	if m != nil {
		return m.tag
	}
	return nil
}

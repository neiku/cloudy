package azbus

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus"
	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus/admin"
)

// Topic wraps the Azure Service Bus topic and its related subscriptions.
type Topic struct {
	ctx       context.Context
	client    *azservicebus.Client
	admin     *admin.Client
	topicName string
	prop      *admin.TopicProperties
	sendMu    sync.Mutex
	sender    *azservicebus.Sender
}

func (t *Topic) String() string {
	s := t.topicName
	if p := t.prop; p != nil {
		s += fmt.Sprintf(`[%dM/%s/%v]`, *p.MaxSizeInMegabytes, *p.DefaultMessageTimeToLive, *p.SupportOrdering)
	}
	return fmt.Sprintf(`📨Topic{%s}`, s)
}

// NewTopic creates a new topic instance with the given connection string and topic name.
func NewTopic(connectionString, topicName string) (*Topic, error) {
	// create topic clients
	ts := time.Now()
	ctx := context.Background()
	client, err := azservicebus.NewClientFromConnectionString(connectionString, nil)
	if err != nil {
		return nil, err
	}
	sender, err := client.NewSender(topicName, nil)
	if err != nil {
		return nil, err
	}
	// create admin client
	adminCli, err := admin.NewClientFromConnectionString(connectionString, nil)
	if err != nil {
		return nil, err
	}
	// get queue properties
	topicProp, err := adminCli.GetTopic(ctx, topicName, nil)
	if err != nil {
		return nil, err
	} else if topicProp == nil {
		return nil, ErrTopicNotFound
	}
	topicRun, err := adminCli.GetTopicRuntimeProperties(ctx, topicName, nil)
	if err != nil {
		return nil, err
	} else if topicRun == nil {
		return nil, ErrTopicNotFound
	}
	st := &Status{
		SizeInBytes:           topicRun.SizeInBytes,
		SubscriptionCount:     topicRun.SubscriptionCount,
		ScheduledMessageCount: topicRun.ScheduledMessageCount,
	}
	log.Debugw("message topic initialized", "topic", topicName, "status", st, "time_elapsed", time.Since(ts))
	// finally return the instance
	return &Topic{
		ctx:       ctx,
		client:    client,
		admin:     adminCli,
		topicName: topicName,
		prop:      &topicProp.TopicProperties,
		sender:    sender,
	}, nil
}

// Close closes the topic and its related subscriptions.
func (t *Topic) Close() error {
	t.sendMu.Lock()
	defer t.sendMu.Unlock()

	if err := t.sender.Close(t.ctx); err != nil {
		return err
	}

	return t.client.Close(t.ctx)
}

// GetStatus returns the status of the topic.
func (t *Topic) GetStatus() (*Status, error) {
	topicRun, err := t.admin.GetTopicRuntimeProperties(t.ctx, t.topicName, nil)
	if err != nil {
		return nil, err
	} else if topicRun == nil {
		return nil, ErrTopicNotFound
	}
	return &Status{
		SizeInBytes:           topicRun.SizeInBytes,
		SubscriptionCount:     topicRun.SubscriptionCount,
		ScheduledMessageCount: topicRun.ScheduledMessageCount,
	}, nil
}

// Send sends a message to the queue immediately. If attrs is nil and data is empty, an error will be returned.
func (t *Topic) Send(data []byte, attrs Attributes) error {
	t.sendMu.Lock()
	defer t.sendMu.Unlock()

	return sendMsg(t.sender, data, attrs)
}

// Schedule schedules a message to be enqueue at the given time, and returns the sequence number of the message.
func (t *Topic) Schedule(data []byte, attrs Attributes, sched time.Time) (int64, error) {
	t.sendMu.Lock()
	defer t.sendMu.Unlock()

	return scheduleMsg(t.sender, data, attrs, sched)
}

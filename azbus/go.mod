module bitbucket.org/neiku/cloudy/azbus

go 1.16

require (
	bitbucket.org/ai69/amoy v0.2.3
	bitbucket.org/neiku/hlog v0.1.2
	github.com/1set/gut v0.0.0-20201117175203-a82363231997
	github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus v1.6.1
	go.uber.org/zap v1.24.0
)

retract v0.0.3 // Dirty hack to fix the duplicate tag version

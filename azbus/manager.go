package azbus

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/ai69/amoy"
	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus"
	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus/admin"
)

// Manager represents a admin client of Azure Service Bus.
type Manager struct {
	ctx                context.Context
	admin              *admin.Client
	prop               *admin.NamespaceProperties
	connStr            string
	lockDuration       *string
	messageTimeToLive  *string
	maxSizeInMegabytes *int32
}

func (m *Manager) String() string {
	return fmt.Sprintf(`📇Manager{%s[%s]}`, m.prop.Name, m.prop.SKU)
}

var (
	defaultLockDuration       = amoy.StringPtr(`PT5M`)
	defaultMessageTimeToLive  = amoy.StringPtr(`P14D`)
	defaultMaxSizeInMegabytes = amoy.Int32Ptr(1024 * 5)
	receiverModePeekLock      = &azservicebus.ReceiverOptions{ReceiveMode: azservicebus.ReceiveModePeekLock}
	receiverModeRecvNDel      = &azservicebus.ReceiverOptions{ReceiveMode: azservicebus.ReceiveModeReceiveAndDelete}
	defaultReceiverMode       = receiverModePeekLock
)

// NewManager creates a new instance of Manager.
func NewManager(connectionString string) (*Manager, error) {
	// create client
	ts := time.Now()
	ctx := context.Background()
	adminCli, err := admin.NewClientFromConnectionString(connectionString, nil)
	if err != nil {
		return nil, err
	}
	// get namespace properties
	prop, err := adminCli.GetNamespaceProperties(ctx, nil)
	if err != nil {
		return nil, err
	}
	log.Debugw("message queue manager initialized", "created_at", prop.CreatedTime, "modified_at", prop.ModifiedTime, "unit", prop.MessagingUnits, "sku", prop.SKU, "time_elapsed", time.Since(ts))
	return &Manager{
		ctx:                ctx,
		admin:              adminCli,
		prop:               &prop.NamespaceProperties,
		connStr:            connectionString,
		lockDuration:       defaultLockDuration,
		messageTimeToLive:  defaultMessageTimeToLive,
		maxSizeInMegabytes: defaultMaxSizeInMegabytes,
	}, nil
}

// SetLockDuration sets the default lock duration of the queue, it will be used when creating new queue or subscription.
func (m *Manager) SetLockDuration(s string) {
	m.lockDuration = amoy.StringPtr(s)
}

// SetMessageTimeToLive sets the default message time to live of the queue, it will be used when creating new queue, topic or subscription.
func (m *Manager) SetMessageTimeToLive(s string) {
	m.messageTimeToLive = amoy.StringPtr(s)
}

// SetMaxSizeMB sets the default max size in megabytes of the queue, it will be used when creating new queue or topic.
func (m *Manager) SetMaxSizeMB(i int32) {
	m.maxSizeInMegabytes = amoy.Int32Ptr(i)
}

// CreateQueue creates a Queue with the given name if not exists, and returns the queue instance.
func (m *Manager) CreateQueue(queueName string) (*Queue, error) {
	// create queue
	ts := time.Now()
	resp, err := m.admin.CreateQueue(m.ctx, queueName, &admin.CreateQueueOptions{
		Properties: &admin.QueueProperties{
			LockDuration:             m.lockDuration,
			MaxSizeInMegabytes:       m.maxSizeInMegabytes,
			DefaultMessageTimeToLive: m.messageTimeToLive,
			// RequiresSession:          amoy.BoolPtr(true),
		},
	})
	if err != nil {
		if strings.Contains(err.Error(), respErr409) {
			return nil, ErrConflict
		}
		return nil, err
	}
	log.Debugw("message queue created", "queue", queueName, "status", resp.Status, "size_mb", resp.MaxSizeInMegabytes, "lock", resp.LockDuration, "ttl", resp.DefaultMessageTimeToLive, "time_elapsed", time.Since(ts))
	// return queue instance
	return NewQueue(m.connStr, resp.QueueName)
}

// DeleteQueue deletes a Queue with the given name if exists.
func (m *Manager) DeleteQueue(queueName string) error {
	ts := time.Now()
	_, err := m.admin.DeleteQueue(m.ctx, queueName, nil)
	if err != nil {
		if strings.Contains(err.Error(), respErr404) {
			return ErrQueueNotFound
		}
		return err
	}
	log.Debugw("message queue deleted", "queue", queueName, "time_elapsed", time.Since(ts))
	return nil
}

// CreateTopic creates a Topic with the given name if not exists, and returns the topic instance.
func (m *Manager) CreateTopic(topicName string) (*Topic, error) {
	// create topic
	ts := time.Now()
	resp, err := m.admin.CreateTopic(m.ctx, topicName, &admin.CreateTopicOptions{
		Properties: &admin.TopicProperties{
			MaxSizeInMegabytes:       m.maxSizeInMegabytes,
			DefaultMessageTimeToLive: m.messageTimeToLive,
			SupportOrdering:          amoy.BoolPtr(true),
		},
	})
	if err != nil {
		if strings.Contains(err.Error(), respErr409) {
			return nil, ErrConflict
		}
		return nil, err
	}
	log.Debugw("message topic created", "topic", topicName, "status", resp.Status, "ttl", resp.DefaultMessageTimeToLive, "time_elapsed", time.Since(ts))
	// return topic instance
	return NewTopic(m.connStr, resp.TopicName)
}

// DeleteTopic deletes a Topic with the given name if exists.
func (m *Manager) DeleteTopic(topicName string) error {
	ts := time.Now()
	_, err := m.admin.DeleteTopic(m.ctx, topicName, nil)
	if err != nil {
		if strings.Contains(err.Error(), respErr404) {
			return ErrTopicNotFound
		}
		return err
	}
	log.Debugw("message topic deleted", "topic", topicName, "time_elapsed", time.Since(ts))
	return nil
}

// CreateSubscription creates a Subscription with the given name if not exists, and returns the subscription instance.
func (m *Manager) CreateSubscription(topicName, subName string) (*Subscription, error) {
	topic, err := NewTopic(m.connStr, topicName)
	if err != nil {
		return nil, err
	}
	return topic.createSubscription(subName, nil, m.lockDuration, m.messageTimeToLive)
}

// CreateSubscriptionWithRule creates a Subscription with the given name and rule if not exists, and returns the subscription instance.
func (m *Manager) CreateSubscriptionWithRule(topicName, subName string, rule *admin.RuleProperties) (*Subscription, error) {
	topic, err := NewTopic(m.connStr, topicName)
	if err != nil {
		return nil, err
	}
	return topic.createSubscription(subName, rule, m.lockDuration, m.messageTimeToLive)
}

// DeleteSubscription deletes a Subscription with the given name if exists.
func (m *Manager) DeleteSubscription(topicName, subName string) error {
	topic, err := NewTopic(m.connStr, topicName)
	if err != nil {
		return err
	}
	return topic.deleteSubscription(subName)
}

package aztable

import (
	"os"

	"bitbucket.org/ai69/amoy"
	"go.uber.org/zap"
)

var log *zap.SugaredLogger

func init() {
	LogOn()
}

func LogOn() {
	logger := amoy.NewJSONLogger("", true)
	logger.SetLogLevel("debug")
	log = logger.LoggerSugared().With(zap.Int("pid", os.Getpid()))
}

func LogOff() {
	log = amoy.NoopZapLogger().Sugar()
}

func LogSet(l *zap.SugaredLogger) {
	log = l
}

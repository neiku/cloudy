module bitbucket.org/neiku/cloudy/aztable

go 1.16

require (
	bitbucket.org/ai69/amoy v0.2.0
	github.com/1set/gut v0.0.0-20201117175203-a82363231997
	github.com/Azure/azure-sdk-for-go v55.8.0+incompatible
	github.com/Azure/go-autorest/autorest v0.11.19 // indirect
	github.com/Azure/go-autorest/autorest/to v0.4.0 // indirect
	github.com/dnaeon/go-vcr v1.2.0 // indirect
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	go.uber.org/zap v1.23.0
)

package aztable

import (
	"errors"

	ys "github.com/1set/gut/ystring"
	az "github.com/Azure/azure-sdk-for-go/storage"
	"go.uber.org/zap"
)

const (
	opTimeoutSec = 15
)

type Service struct {
	cli   az.Client
	tbCli az.TableServiceClient
}

type Table struct {
	svc *Service
	tb  *az.Table
}

// NewService creates a instance for the storage account with given connection string.
func NewService(connectionString string) (*Service, error) {
	var (
		err error
		svc = Service{}
	)
	if svc.cli, err = az.NewClientFromConnectionString(connectionString); err != nil {
		return nil, err
	}
	svc.tbCli = svc.cli.GetTableService()
	return &svc, nil
}

// NewTable creates a instance for the table under the storage account with given name.
func (s *Service) NewTable(tableName string) (*Table, error) {
	if ys.IsBlank(tableName) {
		return nil, errors.New("invalid container name")
	}
	return &Table{
		svc: s,
		tb:  s.tbCli.GetTableReference(tableName),
	}, nil
}

// Create creates the table if it doesn't exist.
func (t *Table) Create() error {
	err := t.tb.Create(opTimeoutSec, az.FullMetadata, nil)
	if err != nil {
		if isAzureStorageServiceError(err, "TableAlreadyExists") {
			log.Warnw("table exists", "name", t.tb.Name, zap.Error(err))
		} else {
			return err
		}
	}
	return nil
}

// CreateEntity inserts entity in the table if it doesn't exist.
func (t *Table) CreateEntity(partitionKey, rowKey string, properties map[string]interface{}) error {
	entity := t.tb.GetEntityReference(partitionKey, rowKey)
	entity.Properties = properties
	if err := entity.Insert(az.MinimalMetadata, &az.EntityOptions{Timeout: opTimeoutSec}); err != nil {
		return err
	}
	return nil
}

// UpsertEntity inserts entity in the table if it doesn't exist, or updates if it exists.
func (t *Table) UpsertEntity(partitionKey, rowKey string, properties map[string]interface{}) error {
	entity := t.tb.GetEntityReference(partitionKey, rowKey)
	entity.Properties = properties
	if err := entity.InsertOrMerge(&az.EntityOptions{Timeout: opTimeoutSec}); err != nil {
		return err
	}
	return nil
}

// ReadEntity reads entity in the table, or returns error if it doesn't exist.
func (t *Table) ReadEntity(partitionKey, rowKey string) (map[string]interface{}, error) {
	entity := t.tb.GetEntityReference(partitionKey, rowKey)
	if err := entity.Get(opTimeoutSec, az.MinimalMetadata, nil); err != nil {
		return nil, err
	}
	return entity.Properties, nil
}

// DeleteEntity deletes entity in the table.
func (t *Table) DeleteEntity(partitionKey, rowKey string) error {
	entity := t.tb.GetEntityReference(partitionKey, rowKey)
	if err := entity.Delete(true, nil); err != nil {
		if IsErrResourceNotFound(err) {
			log.Warnw("entity not exists", "part_key", partitionKey, "row_key", rowKey, zap.Error(err))
		} else {
			return err
		}
	}
	return nil
}

// IsErrResourceNotFound indicates if the error is ResourceNotFound.
func IsErrResourceNotFound(err error) bool {
	return isAzureStorageServiceError(err, "ResourceNotFound")
}

// IsErrEntityAlreadyExists indicates if the error is EntityAlreadyExists.
func IsErrEntityAlreadyExists(err error) bool {
	return isAzureStorageServiceError(err, "EntityAlreadyExists")
}

func isAzureStorageServiceError(err error, s string) bool {
	if ae, ok := err.(az.AzureStorageServiceError); ok {
		return ae.Code == s
	}
	return false
}

package azblob

import (
	"os"

	"bitbucket.org/neiku/hlog"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var log *hlog.Logger

func init() {
	LogOn()
}

func LogOn() {
	log = hlog.NewLogger(hlog.WithLevel(zapcore.DebugLevel)).With(zap.Int("pid", os.Getpid()))
}

func LogOff() {
	log = hlog.NewNoopLogger()
}

func LogSet(l *hlog.Logger) {
	log = l
}

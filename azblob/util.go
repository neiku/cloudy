package azblob

import (
	"crypto/md5"
	"io"
	"os"
	"time"

	azb "github.com/Azure/azure-storage-blob-go/azblob"
)

var (
	emptyStr             string
	connTimeout          = time.Second * 20
	uploadBlobSize       = int64(2 * 1024 * 1024) // 2MB
	uploadParallelism    = uint16(8)
	blobLeaseDurationSec = int32(60)
	blobLeaseRenewIntSec = int32(25)
)

func errorString(err error) string {
	if err == nil {
		return emptyStr
	} else if serr, ok := err.(azb.StorageError); ok {
		return string(serr.ServiceCode())
	}
	return err.Error()
}

func isStorageErrorCode(err error, serviceCodes ...azb.ServiceCodeType) bool {
	if serr, ok := err.(azb.StorageError); ok {
		sc := serr.ServiceCode()
		for _, c := range serviceCodes {
			if sc == c {
				return true
			}
		}
	}
	return false
}

func fileMD5(filePath string) ([]byte, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	hash := md5.New()

	if _, err = io.Copy(hash, file); err != nil {
		return nil, err
	}
	return hash.Sum(nil), nil
}

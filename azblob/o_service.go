package azblob

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"bitbucket.org/neiku/cloudy/azblob/util"
	ys "github.com/1set/gut/ystring"
	azp "github.com/Azure/azure-pipeline-go/pipeline"
	azb "github.com/Azure/azure-storage-blob-go/azblob"
	"go.uber.org/zap"
)

type Resource interface {
	fmt.Stringer
	Exist() bool
}

type Service struct {
	name   string
	azPipe azp.Pipeline
	azSvc  azb.ServiceURL
}

// NewService creates a instance for the storage account with given connection string.
func NewService(connectionString string) (*Service, error) {
	opt, err := util.ParseConnectionString(connectionString)
	if err != nil {
		return nil, err
	}

	credential, err := azb.NewSharedKeyCredential(opt.AccountName, opt.AccountKey)
	if err != nil {
		return nil, err
	}

	ppl := azb.NewPipeline(credential, azb.PipelineOptions{
		Retry: azb.RetryOptions{
			// upload/download timeout for a single block
			// if blob size < 256MB, it will be uploaded in a single block,
			// that's the case when default timeout 60s is not enough over a slow network
			TryTimeout: 10 * time.Minute,
		},
	})
	rawURL, err := url.Parse(opt.Endpoint)
	if err != nil {
		return nil, err
	}

	serviceURL := azb.NewServiceURL(*rawURL, ppl)
	return &Service{name: opt.AccountName, azPipe: ppl, azSvc: serviceURL}, nil
}

func (s *Service) Name() string {
	return s.name
}

func (s *Service) String() string {
	return fmt.Sprintf("Service{%s}", util.TrimURLProtocol(s.azSvc.String()))
}

// Exist checks if this storage account exists and is available to use.
func (s *Service) Exist() bool {
	ctx, cancel := context.WithTimeout(context.Background(), connTimeout)
	defer cancel()

	resp, err := s.azSvc.GetAccountInfo(ctx)
	if resp != nil {
		log.Debugw("check service account exists",
			zap.String("target", s.String()),
			zap.String("status", resp.Status()),
			zap.String("sku", string(resp.SkuName())),
			zap.String("kind", string(resp.AccountKind())),
			zap.String("version", resp.Version()),
		)
		return err == nil && resp.StatusCode() == http.StatusOK
	} else {
		log.Debugw("check service account is not available",
			zap.String("target", s.String()),
			zap.String("error", errorString(err)),
		)
		return false
	}
}

// NewContainer creates a instance for the container under the storage account with given name.
func (s *Service) NewContainer(containerName string) (*Container, error) {
	if ys.IsBlank(containerName) {
		return nil, errors.New("invalid container name")
	}
	return &Container{service: s, azCtn: s.azSvc.NewContainerURL(containerName)}, nil
}

// ListContainer is actually an alias of ListContainerWithPrefix() of Service without prefix.
func (s *Service) ListContainer() ([]*Container, error) {
	return s.ListContainerWithPrefix(emptyStr)
}

// ListContainerWithPrefix lists all containers under the storage account with the given prefix.
func (s *Service) ListContainerWithPrefix(prefix string) ([]*Container, error) {
	segOpt := azb.ListContainersSegmentOptions{Prefix: prefix}
	var containers []*Container
	for marker := (azb.Marker{}); marker.NotDone(); {
		ctx := context.Background()
		listContainer, err := s.azSvc.ListContainersSegment(ctx, marker, segOpt)
		if err != nil {
			return containers, err
		}
		marker = listContainer.NextMarker

		for _, item := range listContainer.ContainerItems {
			c, err := s.NewContainer(item.Name)
			if err != nil {
				return nil, err
			}
			containers = append(containers, c)
		}
	}
	return containers, nil
}

// CreateContainer is actually an alias of Create() of Container.
func (s *Service) CreateContainer(containerName string) (*Container, error) {
	if c, err := s.NewContainer(containerName); err != nil {
		return nil, err
	} else if err := c.Create(); err != nil {
		return nil, err
	} else {
		return c, err
	}
}

// DestroyContainer is actually an alias of Destroy() of Container.
func (s *Service) DestroyContainer(containerName string) error {
	if c, err := s.NewContainer(containerName); err != nil {
		return err
	} else {
		return c.Destroy()
	}
}

package azblob

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"bitbucket.org/neiku/cloudy/azblob/util"
	ys "github.com/1set/gut/ystring"
	azb "github.com/Azure/azure-storage-blob-go/azblob"
	"go.uber.org/zap"
)

type Container struct {
	service *Service
	azCtn   azb.ContainerURL
}

// NewContainer is actually an alias of NewContainer() of Service.
func NewContainer(connectionString, containerName string) (*Container, error) {
	svc, err := NewService(connectionString)
	if err != nil {
		return nil, err
	}
	return svc.NewContainer(containerName)
}

func (c *Container) String() string {
	return fmt.Sprintf("Container{%s}", util.TrimURLProtocol(c.azCtn.String()))
}

// Exist checks if this container exists and is available to use.
func (c *Container) Exist() bool {
	ctx, cancel := context.WithTimeout(context.Background(), connTimeout)
	defer cancel()

	resp, err := c.azCtn.GetProperties(ctx, azb.LeaseAccessConditions{})
	if resp != nil {
		log.Debugw("check container exists",
			zap.String("target", c.String()),
			zap.String("status", resp.Status()),
			zap.Time("last_modified", resp.LastModified()),
			zap.String("lease_state", string(resp.LeaseState())),
			zap.String("lease_status", string(resp.LeaseStatus())),
			zap.String("lease_duration", string(resp.LeaseDuration())),
		)
		return err == nil && resp.StatusCode() == http.StatusOK
	} else {
		log.Debugw("check container is not available",
			zap.String("target", c.String()),
			zap.String("error", errorString(err)),
		)
		return false
	}
}

// Create creates the container if it doesn't exist.
func (c *Container) Create() error {
	ctx := context.Background()
	_, err := c.azCtn.Create(ctx, azb.Metadata{}, azb.PublicAccessNone)
	if err != nil {
		if isStorageErrorCode(err, azb.ServiceCodeContainerAlreadyExists) {
			log.Warnw("attempt to create existing container", zap.String("container", c.String()))
		} else {
			return fmt.Errorf("Container-Create: %s", errorString(err))
		}
	}
	return nil
}

// Destroy deletes the container if it exists.
func (c *Container) Destroy() error {
	ctx := context.Background()
	_, err := c.azCtn.Delete(ctx, azb.ContainerAccessConditions{})
	if err != nil {
		if isStorageErrorCode(err, azb.ServiceCodeContainerNotFound) {
			log.Warnw("attempt to destroy non-existing container", zap.String("container", c.String()))
		} else {
			return fmt.Errorf("Container-Destroy: %s", errorString(err))
		}
	}
	return nil
}

// NewBlob creates a instance for the blob under the container with given name.
func (c *Container) NewBlob(blobName string) (*Blob, error) {
	if ys.IsBlank(blobName) {
		return nil, errors.New("invalid blob name")
	}
	return &Blob{container: c, azBob: c.azCtn.NewBlockBlobURL(blobName)}, nil
}

// ListBlob is actually an alias of ListBlobWithPrefix() of Container without prefix.
func (c *Container) ListBlob() ([]*Blob, error) {
	return c.ListBlobWithPrefix(emptyStr)
}

// ListBlobWithPrefix lists all blobs under the container with the given prefix.
func (c *Container) ListBlobWithPrefix(prefix string) ([]*Blob, error) {
	segOpt := azb.ListBlobsSegmentOptions{Prefix: prefix}
	var blobs []*Blob
	for marker := (azb.Marker{}); marker.NotDone(); {
		ctx := context.Background()
		listBlob, err := c.azCtn.ListBlobsFlatSegment(ctx, marker, segOpt)
		if err != nil {
			return blobs, err
		}
		marker = listBlob.NextMarker

		for _, item := range listBlob.Segment.BlobItems {
			c, err := c.NewBlob(item.Name)
			if err != nil {
				return nil, err
			}
			blobs = append(blobs, c)
		}
	}
	return blobs, nil
}

package azblob

import (
	"context"
	"sync"
	"time"

	"bitbucket.org/ai69/amoy"
	azb "github.com/Azure/azure-storage-blob-go/azblob"
	guuid "github.com/google/uuid"
	"go.uber.org/zap"
)

// leaseKeeper is not thread-safe, use with the lock.
type leaseKeeper struct {
	sync.RWMutex
	leaseID     string
	cancelRenew chan interface{}
}

func (b *Blob) getLeaseID() string {
	b.lsKeep.RLock()
	defer b.lsKeep.RUnlock()
	return b.lsKeep.LeaseID()
}

func (lk *leaseKeeper) Reset() {
	lk.leaseID = guuid.New().String()
	lk.cancelRenew = make(chan interface{}, 2)
}

func (lk *leaseKeeper) Clear() {
	lk.leaseID = emptyStr
	lk.cancelRenew = nil
}

func (lk *leaseKeeper) LeaseID() string {
	return lk.leaseID
}

func (lk *leaseKeeper) StartOnBlob(azBob azb.BlockBlobURL) {
	go lk.keepRenew(azBob)
}

func (lk *leaseKeeper) Stop() {
	close(lk.cancelRenew)
}

func (lk *leaseKeeper) keepRenew(azBob azb.BlockBlobURL) {
	var (
		leaseID = lk.leaseID
		cancel  = lk.cancelRenew
		log     = amoy.NoopZapLogger().Sugar()
		//log        = log.With(zap.String("target", b.String()), zap.String("lease_id", leaseID))
		ticker     = time.NewTicker(time.Duration(blobLeaseRenewIntSec) * time.Second)
		times      = 0
		shouldQuit = false
	)

	for {
		select {
		case <-ticker.C:
			times++
			ctx := context.Background()
			resp, err := azBob.RenewLease(ctx, leaseID, azb.ModifiedAccessConditions{})
			if err != nil {
				shouldQuit = isStorageErrorCode(err,
					azb.ServiceCodeLeaseIsBrokenAndCannotBeRenewed,
					azb.ServiceCodeContainerNotFound, azb.ServiceCodeBlobNotFound,
					azb.ServiceCodeLeaseIDMismatchWithLeaseOperation)
				log.Warnw("fail to renew lease",
					zap.Int("times", times),
					zap.String("error", errorString(err)),
					zap.Bool("should_quit", shouldQuit),
				)
			}
			if resp != nil {
				log.Debugw("renewed lease",
					zap.Int("times", times),
					zap.String("version", resp.Version()),
					zap.String("status", resp.Status()),
					zap.Time("last_modified", resp.LastModified()),
				)
			}
		case <-cancel:
			shouldQuit = true
			log.Debugw("got signal to quit renewing lease")
		}

		if shouldQuit {
			ticker.Stop()
			break
		}
	}
}

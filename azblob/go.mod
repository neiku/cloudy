module bitbucket.org/neiku/cloudy/azblob

go 1.16

require (
	bitbucket.org/ai69/amoy v0.2.0
	bitbucket.org/neiku/cloudy/util v0.0.1
	bitbucket.org/neiku/hlog v0.1.2
	github.com/1set/gut v0.0.0-20201117175203-a82363231997
	github.com/Azure/azure-pipeline-go v0.2.3
	github.com/Azure/azure-storage-blob-go v0.14.0
	github.com/google/uuid v1.3.0
	go.uber.org/zap v1.23.0
)

package azblob

import (
	"context"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"strings"

	"bitbucket.org/neiku/cloudy/util"
	yo "github.com/1set/gut/yos"
	ys "github.com/1set/gut/ystring"
	"github.com/Azure/azure-pipeline-go/pipeline"
	azb "github.com/Azure/azure-storage-blob-go/azblob"
	"go.uber.org/zap"
)

func (b *Blob) UploadContent(ctx context.Context, data []byte) error {
	var reporter ProgressReporter
	if b.reportProg != nil {
		reporter = b.reportProg
	} else {
		ps := util.NewProgressLogSampler(uint64(len(data)))
		totalSize := ps.TotalSize()
		log.Debugw("will upload content as blob",
			zap.String("target", b.String()),
			zap.Int64("content_len", int64(len(data))),
			zap.String("total_size", totalSize),
		)
		reporter = func(bytesTransferred int64) {
			if !ps.Sample(uint64(bytesTransferred)) {
				return
			}
			log.Debugw("upload content in progress",
				zap.String("target", b.String()),
				zap.String("total_size", totalSize),
				zap.String("sent_size", ps.CurrentSize()),
				zap.String("speed", ps.Speed()),
				zap.String("progress", ps.Progress()),
				zap.String("time_spent", ps.TimeCost()),
			)
		}
	}

	uploadBlobOptions := azb.UploadToBlockBlobOptions{
		BlockSize:       uploadBlobSize,
		Progress:        pipeline.ProgressReceiver(reporter),
		BlobHTTPHeaders: azb.BlobHTTPHeaders{},
		AccessConditions: azb.BlobAccessConditions{
			LeaseAccessConditions: azb.LeaseAccessConditions{LeaseID: b.getLeaseID()},
		},
		Parallelism: uploadParallelism,
	}
	if b.contentType != "" {
		uploadBlobOptions.BlobHTTPHeaders.ContentType = b.contentType
	} else if contentType := util.InferContentTypeByName(b.URL()); ys.IsNotEmpty(contentType) {
		uploadBlobOptions.BlobHTTPHeaders.ContentType = contentType
	} else {
		uploadBlobOptions.BlobHTTPHeaders.ContentType = util.InferContentTypeByData(data)
	}

	resp, err := azb.UploadBufferToBlockBlob(ctx, data, b.azBob.ToBlockBlobURL(), uploadBlobOptions)
	if err != nil {
		return fmt.Errorf("Blob-Upload-Content: %s", errorString(err))
	}

	log.Debugw("uploaded content as blob",
		zap.String("target", b.String()),
		zap.Time("last_modified", resp.LastModified()),
	)
	return nil
}

func (b *Blob) UploadFile(ctx context.Context, fileName string) error {
	return b.UploadFileWithMD5(ctx, fileName, nil)
}

func (b *Blob) UploadFileWithMD5(ctx context.Context, fileName string, md5 []byte) error {
	file, err := os.Open(fileName)
	if err != nil {
		return err
	}
	defer func() { _ = file.Close() }()

	stat, err := file.Stat()
	if err != nil {
		return err
	}

	var reporter ProgressReporter
	if b.reportProg != nil {
		reporter = b.reportProg
	} else {
		ps := util.NewProgressLogSampler(uint64(stat.Size()))
		totalSize := ps.TotalSize()
		log.Debugw("will upload file as blob",
			zap.String("file", stat.Name()),
			zap.String("target", b.String()),
			zap.Int64("content_len", int64(stat.Size())),
			zap.String("total_size", totalSize),
		)
		reporter = func(bytesTransferred int64) {
			if !ps.Sample(uint64(bytesTransferred)) {
				return
			}
			log.Debugw("upload file in progress",
				zap.String("target", b.String()),
				zap.String("total_size", totalSize),
				zap.String("sent_size", ps.CurrentSize()),
				zap.String("speed", ps.Speed()),
				zap.String("progress", ps.Progress()),
				zap.String("time_spent", ps.TimeCost()),
			)
		}
	}

	uploadBlobOptions := azb.UploadToBlockBlobOptions{
		BlockSize: uploadBlobSize,
		Progress:  pipeline.ProgressReceiver(reporter),
		BlobHTTPHeaders: azb.BlobHTTPHeaders{
			ContentMD5: md5,
		},
		AccessConditions: azb.BlobAccessConditions{
			LeaseAccessConditions: azb.LeaseAccessConditions{LeaseID: b.getLeaseID()},
		}, Parallelism: uploadParallelism,
	}
	if b.contentType != "" {
		uploadBlobOptions.BlobHTTPHeaders.ContentType = b.contentType
	} else {
		uploadBlobOptions.BlobHTTPHeaders.ContentType = util.InferContentTypeByNameAndData(fileName)
	}

	resp, err := azb.UploadFileToBlockBlob(ctx, file, b.azBob.ToBlockBlobURL(), uploadBlobOptions)
	if err != nil {
		return fmt.Errorf("Blob-Upload-File: %s", errorString(err))
	}

	log.Debugw("uploaded file as blob",
		zap.String("target", b.String()),
		zap.Time("last_modified", resp.LastModified()),
	)
	return nil
}

func (b *Blob) UploadFileIfNewer(ctx context.Context, fileName string) error {
	var (
		blobHash      string
		fileHash      string
		fileHashBytes []byte
	)

	if yo.ExistFile(fileName) {
		var err error
		fileHashBytes, err = fileMD5(fileName)
		if err != nil {
			return err
		}
		fileHash = hex.EncodeToString(fileHashBytes)
	}

	resp, err := b.azBob.GetProperties(ctx, azb.BlobAccessConditions{}, azb.ClientProvidedKeyOptions{})
	if err != nil {
		if isStorageErrorCode(err, azb.ServiceCodeBlobNotFound) {
			return b.UploadFileWithMD5(ctx, fileName, fileHashBytes)
		}
		return fmt.Errorf("Blob-Get-Properties: %s", errorString(err))
	} else {
		blobHash = hex.EncodeToString(resp.ContentMD5())
	}

	l := log.With(
		zap.String("file", fileName),
		zap.String("target", b.String()),
		zap.String("content_type", resp.ContentType()),
		zap.Int64("content_len", resp.ContentLength()),
		zap.String("file_hash_md5", fileHash),
		zap.String("blob_hash_md5", blobHash),
		zap.Time("last_modified", resp.LastModified()),
	)
	if strings.EqualFold(fileHash, blobHash) {
		l.Debugw("skip uploading blob with same content")
		return nil
	} else {
		l.Debugw("check online blob has different content")
		return b.UploadFileWithMD5(ctx, fileName, fileHashBytes)
	}
}

func (b *Blob) UploadStream(ctx context.Context, stream io.ReadSeeker) error {
	log.Debugw("will upload stream as blob",
		zap.String("target", b.String()),
	)

	resp, err := azb.UploadStreamToBlockBlob(ctx, stream, b.azBob.ToBlockBlobURL(), azb.UploadStreamToBlockBlobOptions{
		BufferSize: int(uploadBlobSize),
		MaxBuffers: int(uploadParallelism),
		//BlobHTTPHeaders:  azb.BlobHTTPHeaders{},
		AccessConditions: azb.BlobAccessConditions{
			LeaseAccessConditions: azb.LeaseAccessConditions{LeaseID: b.getLeaseID()},
		}})
	if err != nil {
		return fmt.Errorf("Blob-Upload-Stream: %s", errorString(err))
	}

	log.Debugw("uploaded stream as blob",
		zap.String("target", b.String()),
		zap.Time("last_modified", resp.LastModified()),
	)
	return nil
}

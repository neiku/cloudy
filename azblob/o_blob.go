package azblob

import (
	"context"
	"encoding/hex"
	"errors"
	"fmt"
	"net/http"

	"bitbucket.org/neiku/cloudy/azblob/util"
	ys "github.com/1set/gut/ystring"
	azb "github.com/Azure/azure-storage-blob-go/azblob"
	"go.uber.org/zap"
)

// ProgressReporter defines the signature of a callback function invoked as progress is reported.
type ProgressReporter func(bytesTransferred int64)

type Blob struct {
	container   *Container
	azBob       azb.BlockBlobURL
	lsKeep      leaseKeeper
	reportProg  ProgressReporter
	contentType string
}

// NewBlob is actually an alias of NewBlob() of Container.
func NewBlob(connectionString, containerName, blobName string) (*Blob, error) {
	cnt, err := NewContainer(connectionString, containerName)
	if err != nil {
		return nil, err
	}
	return cnt.NewBlob(blobName)
}

func (b *Blob) String() string {
	return fmt.Sprintf("Blob{%s}", util.TrimURLProtocol(b.azBob.String()))
}

// URL returns the URL of the blob.
func (b *Blob) URL() string {
	return b.azBob.BlobURL.String()
}

// SetProgressReporter sets ProgressReporter of the blob for downloading and uploading.
func (b *Blob) SetProgressReporter(report ProgressReporter) {
	b.reportProg = report
}

// SetContentType sets ContentType of the blob.
func (b *Blob) SetContentType(contentType string) {
	b.contentType = contentType
}

// Exist checks if this blob exists and is available to use.
func (b *Blob) Exist() bool {
	ctx, cancel := context.WithTimeout(context.Background(), connTimeout)
	defer cancel()

	resp, err := b.azBob.GetProperties(ctx, azb.BlobAccessConditions{}, azb.ClientProvidedKeyOptions{})
	if resp != nil {
		log.Debugw("check blob exists",
			zap.String("target", b.String()),
			zap.String("status", resp.Status()),
			zap.String("content_type", resp.ContentType()),
			zap.Int64("content_len", resp.ContentLength()),
			zap.String("hash_md5", hex.EncodeToString(resp.ContentMD5())),
			zap.Time("last_modified", resp.LastModified()),
			zap.String("lease_state", string(resp.LeaseState())),
			zap.String("lease_status", string(resp.LeaseStatus())),
			zap.String("lease_duration", string(resp.LeaseDuration())),
		)
		return err == nil && resp.StatusCode() == http.StatusOK
	} else {
		log.Debugw("check blob is not available",
			zap.String("target", b.String()),
			zap.String("error", errorString(err)),
		)
		return false
	}
}

// GetProperties returns the properties of the blob
func (b *Blob) GetProperties() (*azb.BlobGetPropertiesResponse, error) {
	return b.azBob.GetProperties(context.Background(), azb.BlobAccessConditions{}, azb.ClientProvidedKeyOptions{})
}

// Size returns the size of the blob.
func (b *Blob) Size() (int64, error) {
	resp, err := b.azBob.GetProperties(context.Background(), azb.BlobAccessConditions{}, azb.ClientProvidedKeyOptions{})
	if err != nil {
		return 0, err
	}
	return resp.ContentLength(), nil
}

// Destroy deletes the blob if it exists.
func (b *Blob) Destroy() error {
	ctx := context.Background()
	_, err := b.azBob.Delete(ctx, azb.DeleteSnapshotsOptionInclude, azb.BlobAccessConditions{LeaseAccessConditions: azb.LeaseAccessConditions{LeaseID: b.getLeaseID()}})
	if err != nil {
		if isStorageErrorCode(err, azb.ServiceCodeBlobNotFound) {
			log.Warnw("attempt to destroy non-existing blob", zap.String("container", b.String()))
		} else {
			return fmt.Errorf("Blob-Destroy: %s", errorString(err))
		}
	}
	return nil
}

// TODO: fix data race issue
func (b *Blob) Lock() error {
	if leaseID := b.getLeaseID(); ys.IsNotBlank(leaseID) {
		return fmt.Errorf("already acquired lease: %v", leaseID)
	}

	b.lsKeep.Lock()
	defer b.lsKeep.Unlock()

	b.lsKeep.Reset()
	leaseID := b.lsKeep.LeaseID()
	log := log.With(zap.String("target", b.String()), zap.String("lease_id", leaseID))

	ctx := context.Background()
	resp, err := b.azBob.AcquireLease(ctx, leaseID, blobLeaseDurationSec, azb.ModifiedAccessConditions{})
	if err != nil {
		errStr := errorString(err)
		log.Warnw("fail to acquire lease",
			zap.String("error", errStr),
		)
		b.lsKeep.Clear()
		return fmt.Errorf("Blob-Lease-Acquire: %s", errStr)
	}
	if resp != nil {
		log.Debugw("acquired lease",
			zap.String("version", resp.Version()),
			zap.String("status", resp.Status()),
			zap.Time("last_modified", resp.LastModified()),
		)
		b.lsKeep.StartOnBlob(b.azBob)
	}
	return nil
}

// TODO: fix data race issue
func (b *Blob) Unlock() error {
	leaseID := b.getLeaseID()
	if ys.IsBlank(leaseID) {
		return errors.New("no acquired lease")
	}

	b.lsKeep.Lock()
	defer b.lsKeep.Unlock()

	b.lsKeep.Stop()
	b.lsKeep.Clear()

	log := log.With(zap.String("target", b.String()))
	ctx := context.Background()
	resp, err := b.azBob.ReleaseLease(ctx, leaseID, azb.ModifiedAccessConditions{})
	if err != nil {
		errStr := errorString(err)
		log.Warnw("fail to release lease",
			zap.String("error", errStr),
		)
		return fmt.Errorf("Blob-Lease-Release: %s", errStr)
	}
	if resp != nil {
		log.Debugw("released lease",
			zap.String("version", resp.Version()),
			zap.String("status", resp.Status()),
			zap.Time("last_modified", resp.LastModified()),
		)
	}
	return nil
}

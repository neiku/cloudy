package util

import (
	"errors"
	"fmt"
	"net/http"
	"regexp"
	"strings"

	ys "github.com/1set/gut/ystring"
)

const (
	// defaultEndpointSuffix is the domain name used for storage requests in the public cloud when a default client is created.
	defaultEndpointSuffix = "core.windows.net"

	// storageEmulatorAccountName is the fixed storage account used by Azure Storage Emulator
	storageEmulatorAccountName = "devstoreaccount1"

	// storageEmulatorAccountKey is the the fixed storage account used by Azure Storage Emulator
	storageEmulatorAccountKey = "Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw=="

	blobServiceName  = "blob"
	tableServiceName = "table"
	queueServiceName = "queue"
	fileServiceName  = "file"

	connectionStringAccountName      = "accountname"
	connectionStringAccountKey       = "accountkey"
	connectionStringEndpointSuffix   = "endpointsuffix"
	connectionStringEndpointProtocol = "defaultendpointsprotocol"

	connectionStringBlobEndpoint  = "blobendpoint"
	connectionStringFileEndpoint  = "fileendpoint"
	connectionStringQueueEndpoint = "queueendpoint"
	connectionStringTableEndpoint = "tableendpoint"
	connectionStringSAS           = "sharedaccesssignature"
)

var (
	validStorageAccount     = regexp.MustCompile("^[0-9a-z]{3,24}$")
	defaultValidStatusCodes = []int{
		http.StatusRequestTimeout,      // 408
		http.StatusInternalServerError, // 500
		http.StatusBadGateway,          // 502
		http.StatusServiceUnavailable,  // 503
		http.StatusGatewayTimeout,      // 504
	}

	urlProtocolRegex = regexp.MustCompile(`(?i)^https?://`)
)

// ConnectionOptions represents structured options for connection string.
type ConnectionOptions struct {
	AccountName string
	AccountKey  string
	Endpoint    string
}

// ParseConnectionString extracts options from the connection string.
func ParseConnectionString(input string) (*ConnectionOptions, error) {
	// build a map of connection string key/value pairs
	parts := map[string]string{}
	for _, pair := range strings.Split(input, ";") {
		if pair == "" {
			continue
		}

		equalDex := strings.IndexByte(pair, '=')
		if equalDex <= 0 {
			return nil, fmt.Errorf("invalid connection segment %q", pair)
		}

		value := strings.TrimSpace(pair[equalDex+1:])
		key := strings.TrimSpace(strings.ToLower(pair[:equalDex]))
		parts[key] = value
	}

	if ys.IsNotEmpty(parts[connectionStringSAS]) {
		return nil, errors.New("shared access signature is not supported")
	}

	accountName := parts[connectionStringAccountName]
	if !validStorageAccount.MatchString(accountName) {
		return nil, fmt.Errorf("invalid account name %q", accountName)
	} else if accountName == storageEmulatorAccountName {
		return nil, errors.New("storage emulator is not supported")
	}

	protocol := "https"
	if ys.IsNotEmpty(parts[connectionStringEndpointProtocol]) {
		protocol = parts[connectionStringEndpointProtocol]
	}

	endpointSuffix := defaultEndpointSuffix
	if ys.IsNotEmpty(parts[connectionStringEndpointSuffix]) {
		endpointSuffix = parts[connectionStringEndpointSuffix]
	}

	endpoint := fmt.Sprintf("%s://%s",
		protocol,
		strings.Join([]string{
			accountName,
			blobServiceName,
			endpointSuffix,
		}, "."))

	return &ConnectionOptions{
		AccountName: accountName,
		AccountKey:  parts[connectionStringAccountKey],
		Endpoint:    endpoint,
	}, nil
}

func TrimURLProtocol(s string) string {
	return string(urlProtocolRegex.ReplaceAll([]byte(s), nil))
}

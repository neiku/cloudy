package azblob

import (
	"bufio"
	"bytes"
	"context"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"strings"

	"bitbucket.org/neiku/cloudy/util"
	yh "github.com/1set/gut/yhash"
	yo "github.com/1set/gut/yos"
	azp "github.com/Azure/azure-pipeline-go/pipeline"
	azb "github.com/Azure/azure-storage-blob-go/azblob"
	"go.uber.org/zap"
)

func (b *Blob) DownloadContent(ctx context.Context) ([]byte, error) {
	data := bytes.Buffer{}
	err := b.DownloadStream(func(stream io.ReadCloser) error {
		return copyStream(ctx, bufio.NewWriter(&data), stream)
		//_, e := data.ReadFrom(stream)
		//return e
	})

	if err != nil {
		return nil, fmt.Errorf("Blob-Download-Content: %s", errorString(err))
	}
	return data.Bytes(), nil
}

func (b *Blob) DownloadFile(ctx context.Context, fileName string) error {
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer func() { _ = file.Close() }()

	err = b.DownloadStream(func(stream io.ReadCloser) error {
		return copyStream(ctx, file, stream)
		//_, e := io.Copy(file, stream)
		//return e
	})
	return err
}

func (b *Blob) DownloadFileIfNewer(ctx context.Context, fileName string) error {
	if yo.NotExist(fileName) {
		return b.DownloadFile(ctx, fileName)
	}

	fileHash, err := yh.FileMD5(fileName)
	if err != nil {
		return err
	}

	resp, err := b.azBob.GetProperties(ctx, azb.BlobAccessConditions{}, azb.ClientProvidedKeyOptions{})
	if err != nil {
		return fmt.Errorf("Blob-Get-Properties: %s", errorString(err))
	}
	blobHash := hex.EncodeToString(resp.ContentMD5())

	l := log.With(
		zap.String("target", b.String()),
		zap.String("content_type", resp.ContentType()),
		zap.Int64("content_len", resp.ContentLength()),
		zap.String("file_hash_md5", fileHash),
		zap.String("blob_hash_md5", blobHash),
		zap.Time("last_modified", resp.LastModified()),
	)
	if strings.EqualFold(fileHash, blobHash) {
		l.Debugw("skip downloading blob with same content")
		return nil
	} else {
		l.Debugw("check local blob has different content")
		return b.DownloadFile(ctx, fileName)
	}
}

func (b *Blob) DownloadStream(handler func(io.ReadCloser) error) error {
	ctx := context.Background()
	response, err := b.azBob.Download(ctx, 0, azb.CountToEnd, azb.BlobAccessConditions{}, false, azb.ClientProvidedKeyOptions{})
	if err != nil {
		return fmt.Errorf("Blob-Download-Stream: %s", errorString(err))
	}

	totalBytes := response.ContentLength()
	log.Debugw("will download blob",
		zap.String("target", b.String()),
		zap.String("content_type", response.ContentType()),
		zap.Int64("content_len", totalBytes),
		zap.String("hash_md5", hex.EncodeToString(response.ContentMD5())),
		zap.Time("last_modified", response.LastModified()),
	)

	stream := response.Body(azb.RetryReaderOptions{MaxRetryRequests: 5})
	if totalBytes > 1024*1024 {
		var reporter ProgressReporter
		if b.reportProg != nil {
			reporter = b.reportProg
		} else {
			ps := util.NewProgressLogSampler(uint64(totalBytes))
			totalSize := ps.TotalSize()
			reporter = func(bytesTransferred int64) {
				if !ps.Sample(uint64(bytesTransferred)) {
					return
				}
				log.Debugw("download in progress",
					zap.String("target", b.String()),
					zap.String("total_size", totalSize),
					zap.String("received_size", ps.CurrentSize()),
					zap.String("speed", ps.Speed()),
					zap.String("progress", ps.Progress()),
					zap.String("time_spent", ps.TimeCost()),
				)
			}
		}
		stream = azp.NewResponseBodyProgress(stream, azp.ProgressReceiver(reporter))
	}
	defer func() { _ = stream.Close() }()

	err = handler(stream)
	return err
}

type readerFunc func(p []byte) (n int, err error)

func (rf readerFunc) Read(p []byte) (n int, err error) { return rf(p) }

func copyStream(ctx context.Context, dst io.Writer, src io.Reader) error {
	// Copy will call the Reader and Writer interface multiple time, in order
	// to copy by chunk (avoiding loading the whole file in memory).
	// I insert the ability to cancel before read time as it is the earliest
	// possible in the call process.
	_, err := io.Copy(dst, readerFunc(func(p []byte) (int, error) {
		// golang non-blocking channel: https://gobyexample.com/non-blocking-channel-operations
		select {
		// if context has been canceled
		case <-ctx.Done():
			// stop process and propagate "context canceled" error
			return 0, ctx.Err()
		default:
			// otherwise just run default io.Reader implementation
			return src.Read(p)
		}
	}))
	return err
}

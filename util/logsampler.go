package util

import (
	"fmt"
	"math"
	"time"
)

// ProgressLogSampler returns a log sampler that samples after every minimal progress percent, interval or block size.
type ProgressLogSampler struct {
	totalBytes   uint64
	startTime    time.Time
	lastTime     time.Time
	lastBytes    uint64
	bytesPerSec  uint64
	lastPercent  float64
	minBlockSize uint64
	minPercent   float64
	minInterval  time.Duration
}

// NewProgressLogSampler returns a ProgressLogSampler instance.
func NewProgressLogSampler(totalBytes uint64) *ProgressLogSampler {
	now := time.Now()
	return &ProgressLogSampler{
		totalBytes:   totalBytes,
		startTime:    now,
		lastTime:     now,
		lastBytes:    0,
		lastPercent:  0,
		minBlockSize: 5 * 1024 * 1024,
		minPercent:   0.05,
		minInterval:  5 * time.Second,
	}
}

func (p *ProgressLogSampler) SetMinimalBlockSize(m uint64) {
	p.minBlockSize = m
}

func (p *ProgressLogSampler) SetMinimalPercent(m float64) {
	p.minPercent = m
}

func (p *ProgressLogSampler) SetMinimalInterval(m time.Duration) {
	p.minInterval = m
}

// Sample indicates if it should sample and records the progress.
func (p *ProgressLogSampler) Sample(receivedSize uint64) bool {
	// must sample rules
	if receivedSize == p.totalBytes || receivedSize-p.lastBytes > p.minBlockSize {
		p.record(receivedSize)
		return true
	}

	// minimal requirements
	if p.totalBytes > 0 && float64(receivedSize)/float64(p.totalBytes)-p.lastPercent < p.minPercent {
		return false
	}
	if time.Since(p.lastTime) < p.minInterval {
		return false
	}
	p.record(receivedSize)
	return true
}

func (p *ProgressLogSampler) record(receivedSize uint64) {
	p.bytesPerSec = uint64(math.Max(0, float64(receivedSize-p.lastBytes)) / time.Since(p.lastTime).Seconds())
	p.lastTime = time.Now()
	p.lastBytes = receivedSize
	if p.totalBytes > 0 {
		p.lastPercent = float64(p.lastBytes) / float64(p.totalBytes)
	}
}

func (p *ProgressLogSampler) Progress() string {
	percentSign := "%"
	if p.lastBytes == 0 {
		return "0" + percentSign
	} else if p.lastBytes == p.totalBytes {
		return "100" + percentSign
	}
	return fmt.Sprintf("%.2f%s", p.lastPercent*100, percentSign)
}

func (p *ProgressLogSampler) Speed() string {
	return HumanizeIBytes(p.bytesPerSec) + "/s"
}

func (p *ProgressLogSampler) CurrentSize() string {
	return HumanizeIBytes(p.lastBytes)
}

func (p *ProgressLogSampler) TotalSize() string {
	return HumanizeIBytes(p.totalBytes)
}

func (p *ProgressLogSampler) TimeCost() string {
	return time.Since(p.startTime).String()
}

package util

import (
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	ys "github.com/1set/gut/ystring"
)

var builtinTypes = map[string]string{
	".7z":   "application/x-7z-compressed",
	".css":  "text/css;",
	".gif":  "image/gif",
	".htm":  "text/html;",
	".html": "text/html;",
	".jpeg": "image/jpeg",
	".jpg":  "image/jpeg",
	".js":   "application/javascript",
	".json": "application/json",
	".mjs":  "application/javascript",
	".mp3":  "audio/mpeg",
	".mp4":  "video/mp4",
	".mpeg": "video/mpeg",
	".pdf":  "application/pdf",
	".png":  "image/png",
	".rar":  "application/vnd.rar",
	".svg":  "image/svg+xml",
	".txt":  "text/plain",
	".wasm": "application/wasm",
	".wav":  "audio/x-wav",
	".webp": "image/webp",
	".xml":  "text/xml;",
	".zip":  "application/zip",
}

// InferContentTypeByData infers the MIME Content-Type of the given data
func InferContentTypeByData(data []byte) string {
	return http.DetectContentType(data)
}

// InferContentTypeByNameAndData infers the MIME Content-Type of the given file.
// It first infers the Content-Type using file extension, then using file content
// if file extension has no associated type.
// fullFilePath should be a valid file path.
func InferContentTypeByNameAndData(fullFilePath string) string {
	if guessedType := InferContentTypeByName(fullFilePath); ys.IsNotEmpty(guessedType) {
		return guessedType
	}

	if file, err := os.Open(fullFilePath); err == nil {
		xfer := make([]byte, 512)
		_, _ = file.Read(xfer)
		return http.DetectContentType(xfer)
	}

	return "application/octet-stream" // fallback
}

// InferContentTypeByName infers the MIME Content-Type of the given file.
// It infers the Content-Type using file extension. When extension has no associated type, it returns "".
// fileName is a string contains file name at the end, it can be a file name, file path or an url.
func InferContentTypeByName(fileName string) string {
	fileExtension := filepath.Ext(fileName)

	// short-circuit for common static website files
	// mime.TypeByExtension takes the registry into account, which is most often undesirable in practice
	if override, ok := builtinTypes[strings.ToLower(fileExtension)]; ok {
		return override
	}

	if guessedType := mime.TypeByExtension(fileExtension); ys.IsNotEmpty(guessedType) {
		return guessedType
	}

	return ""
}

package sry

import (
	"context"

	"github.com/getsentry/sentry-go"
)

// SendMessage sends a message event to sentry.
func SendMessage(ctx context.Context, msg string, options ...ScopeSetter) *sentry.EventID {
	var hub *sentry.Hub
	if ctx != nil {
		hub = sentry.GetHubFromContext(ctx)
	}
	return SendMessageFromHub(hub, msg, options...)
}

// SendMessageFromHub sends a message event to sentry with given hub.
func SendMessageFromHub(hub *sentry.Hub, msg string, options ...ScopeSetter) *sentry.EventID {
	if hub == nil {
		hub = sentry.CurrentHub()
	}
	var eid *sentry.EventID
	hub.WithScope(func(scope *sentry.Scope) {
		for _, setter := range GlobalScopeClone() {
			setter(scope)
		}
		for _, option := range options {
			if option != nil {
				option(scope)
			}
		}
		eid = hub.CaptureMessage(msg)
	})
	return eid
}

// SendException sends an error event to sentry.
func SendException(ctx context.Context, err error, options ...ScopeSetter) *sentry.EventID {
	var hub *sentry.Hub
	if ctx != nil {
		hub = sentry.GetHubFromContext(ctx)
	}
	return SendExceptionFromHub(hub, err, options...)
}

// SendExceptionFromHub sends an error event to sentry with given hub.
func SendExceptionFromHub(hub *sentry.Hub, err error, options ...ScopeSetter) *sentry.EventID {
	if hub == nil {
		hub = sentry.CurrentHub()
	}
	var eid *sentry.EventID
	hub.WithScope(func(scope *sentry.Scope) {
		for _, setter := range GlobalScopeClone() {
			setter(scope)
		}
		for _, option := range options {
			if option != nil {
				option(scope)
			}
		}
		eid = hub.CaptureException(err)
	})
	return eid
}

// ScopeSetter is a function that sets a property of scope.
type ScopeSetter func(s *sentry.Scope)

// ScopeFingerprint is a ScopeSetter that sets a fingerprint of scope.
func ScopeFingerprint(fingerprint []string) ScopeSetter {
	return func(s *sentry.Scope) {
		s.SetFingerprint(fingerprint)
	}
}

// ScopeTag is a ScopeSetter that sets a tag of scope.
func ScopeTag(key, value string) ScopeSetter {
	return func(s *sentry.Scope) {
		s.SetTag(key, value)
	}
}

// ScopeTags is a ScopeSetter that sets tags of scope.
func ScopeTags(tags map[string]string) ScopeSetter {
	return func(s *sentry.Scope) {
		s.SetTags(tags)
	}
}

// ScopeLevel is a ScopeSetter that sets a level of scope.
func ScopeLevel(lvl sentry.Level) ScopeSetter {
	return func(s *sentry.Scope) {
		s.SetLevel(lvl)
	}
}

// ScopeTransaction is a ScopeSetter that sets a transaction of scope.
func ScopeTransaction(transaction string) ScopeSetter {
	return func(s *sentry.Scope) {
		s.SetTransaction(transaction)
	}
}

// ScopeUser is a ScopeSetter that sets a user of scope.
func ScopeUser(user sentry.User) ScopeSetter {
	return func(s *sentry.Scope) {
		s.SetUser(user)
	}
}

// ScopeExtra is a ScopeSetter that sets additional data of scope.
func ScopeExtra(key string, data interface{}) ScopeSetter {
	return func(s *sentry.Scope) {
		s.SetExtra(key, data)
	}
}

// ScopeContextData is a ScopeSetter that sets a list of context of scope.
func ScopeContextData(section string, data sentry.Context) ScopeSetter {
	return func(s *sentry.Scope) {
		s.SetContext(section, data)
	}
}

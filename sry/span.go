package sry

import (
	"context"
	"sync"
	"time"

	"github.com/getsentry/sentry-go"
)

// SpanWrapper wraps sentry.Span to provide a convenient way to start and finish a span.
type SpanWrapper struct {
	*sentry.Span
	doneOnce sync.Once
	scopeSet []ScopeSetter
}

func newSpanWrapper(s *sentry.Span) *SpanWrapper {
	return &SpanWrapper{
		s,
		sync.Once{},
		nil,
	}
}

// StartTransaction starts a transaction with given name and options, if there's no active transaction in context.
// Otherwise, it starts a child span of the existing transaction.
// Note that the function with the same name in sentry-go package returns the existing transaction if there's one,
// which is not the behavior we want.
func StartTransaction(ctx context.Context, name string, options ...SpanSetter) *SpanWrapper {
	// in case of no context
	if ctx == nil {
		ctx = context.Background()
	}
	var core *sentry.Span
	// if already in a transaction, then start a child span
	if trans := sentry.TransactionFromContext(ctx); trans != nil {
		core = trans.StartChild(name)
	} else {
		// start a new transaction, if there's no transaction in context
		core = sentry.StartTransaction(ctx, name)
	}
	// wrap the core span
	sw := newSpanWrapper(core)
	for _, option := range options {
		if option != nil {
			option(sw)
		}
	}
	return sw
}

// StartChild starts a child span with given name and options.
func (s *SpanWrapper) StartChild(opName string, options ...SpanSetter) *SpanWrapper {
	core := s.Span.StartChild(opName)
	sw := newSpanWrapper(core)
	for _, option := range options {
		if option != nil {
			option(sw)
		}
	}
	return sw
}

// Apply applies all SpanSetter to the SpanWrapper.
func (s *SpanWrapper) Apply(options ...SpanSetter) {
	for _, option := range options {
		if option != nil {
			option(s)
		}
	}
}

// Stop sets the end time of Span.
func (s *SpanWrapper) Stop() {
	if s.EndTime.IsZero() {
		start := s.StartTime
		s.EndTime = start.Add(time.Since(start))
	}
}

// Finish is an alias of Complete() to shadow the original function.
func (s *SpanWrapper) Finish() {
	s.Complete()
}

// Complete marks the transaction completed and as successful if it has not been marked.
func (s *SpanWrapper) Complete() {
	s.doneOnce.Do(func() {
		if s.Status != sentry.SpanStatusUndefined {
			s.finishWithStatus(s.Status)
		} else {
			s.finishWithStatus(sentry.SpanStatusOK)
		}
	})
}

// Success marks the transaction completed as successful.
func (s *SpanWrapper) Success() {
	s.doneOnce.Do(func() {
		s.finishWithStatus(sentry.SpanStatusOK)
	})
}

// Failure marks the transaction completed as failed with given status.
func (s *SpanWrapper) Failure(status sentry.SpanStatus) {
	s.doneOnce.Do(func() {
		s.finishWithStatus(status)
	})
}

func (s *SpanWrapper) finishWithStatus(status sentry.SpanStatus) {
	hub := sentry.GetHubFromContext(s.Context())
	if hub == nil {
		hub = sentry.CurrentHub()
	}
	hub.WithScope(func(scope *sentry.Scope) {
		for _, setter := range GlobalScopeClone() {
			setter(scope)
		}
		for _, setter := range s.scopeSet {
			if setter != nil {
				setter(scope)
			}
		}
		s.Status = status
		s.Span.Finish()
	})
}

// SpanSetter is a function that sets a property of Span and its Scope.
type SpanSetter func(s *SpanWrapper)

// SpanTag is a SpanSetter that sets a searchable tag of Span.
func SpanTag(key, value string) SpanSetter {
	return func(span *SpanWrapper) {
		// TODO: limit key, value length
		span.SetTag(key, value)
	}
}

// SpanStatus is a SpanSetter that sets the status of Span.
func SpanStatus(status sentry.SpanStatus) SpanSetter {
	return func(span *SpanWrapper) {
		span.Status = status
	}
}

// SpanDescription is a SpanSetter that sets the description of Span.
func SpanDescription(description string) SpanSetter {
	return func(span *SpanWrapper) {
		span.Description = description
	}
}

// SpanLevel is a SpanSetter that sets the level of Span.
// It works only on transaction/root span, since it's based on scope setter.
func SpanLevel(level sentry.Level) SpanSetter {
	return func(span *SpanWrapper) {
		span.scopeSet = append(span.scopeSet, ScopeLevel(level))
	}
}

// SpanTraceID is a SpanSetter that sets the trace ID of Span.
func SpanTraceID(t sentry.TraceID) SpanSetter {
	return func(span *SpanWrapper) {
		span.TraceID = t
	}
}

// SpanTraceIDString is a SpanSetter that sets the trace ID of Span from raw string.
func SpanTraceIDString(ts string) SpanSetter {
	return func(span *SpanWrapper) {
		span.TraceID = NewTraceID(ts)
	}
}

// SpanUser is a SpanSetter that sets the user of Span.
// It works only on transaction/root span, since it's based on scope setter.
func SpanUser(user sentry.User) SpanSetter {
	return func(span *SpanWrapper) {
		span.scopeSet = append(span.scopeSet, ScopeUser(user))
	}
}

// SpanExtra is a SpanSetter that sets a key-value as additional data of Span, existing key will be overwritten.
func SpanExtra(key string, data interface{}) SpanSetter {
	return func(span *SpanWrapper) {
		if span.Data == nil {
			span.Data = make(map[string]interface{})
		}
		span.Data[key] = data
	}
}

// SpanContextData is a SpanSetter that sets a section of context data of Span, existing section will be overwritten.
// It works only on transaction/root span, since it's based on scope setter.
func SpanContextData(section string, data sentry.Context) SpanSetter {
	return func(span *SpanWrapper) {
		span.scopeSet = append(span.scopeSet, ScopeContextData(section, data))
	}
}

// SpanScopeSet is a SpanSetter that append a ScopeSetter to Span.
// It works only on transaction/root span, since it's a scope setter.
func SpanScopeSet(setter ScopeSetter) SpanSetter {
	return func(span *SpanWrapper) {
		span.scopeSet = append(span.scopeSet, setter)
	}
}

// SpanOptionSet is a SpanSetter that apply a SpanOption to Span.
func SpanOptionSet(option sentry.SpanOption) SpanSetter {
	return func(span *SpanWrapper) {
		option(span.Span)
	}
}

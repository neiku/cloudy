module bitbucket.org/neiku/cloudy/sry

go 1.16

require (
	github.com/getsentry/sentry-go v0.15.0
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/text v0.4.0 // indirect
)

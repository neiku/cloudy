package sry_test

import (
	"context"
	"errors"
	"fmt"

	"bitbucket.org/neiku/cloudy/sry"
	"github.com/getsentry/sentry-go"
)

// This function demonstrates how to use the sry package to record a transaction.
func Example_transaction() {
	_ = sry.InitSentryDebug("https://abc@123.ingest.sentry.io/456")
	defer sry.Flush()

	// set a global scope setter
	sry.GlobalScopePush(func(s *sentry.Scope) {
		s.SetTag("example", "test")
	})

	// start a transaction
	tx := sry.StartTransaction(
		context.Background(),
		"make_affogato",
		sry.SpanUser(sentry.User{IPAddress: "{{auto}}"}),
	)
	// just in case
	defer tx.Complete()

	// start a span
	s1 := tx.StartChild("chill_glass",
		sry.SpanTag("temperature", "cold"),
		sry.SpanExtra("tool", []string{"fridge"}),
	)
	// doing ...
	s1.Success()

	// start another span
	s2 := tx.StartChild("add_gelato")
	s2.Apply(sry.SpanTag("flavor", "vanilla"))
	// doing ...
	s2.Success()

	// start the last span
	s3 := tx.StartChild("add_espresso", sry.SpanTag("strength", "strong"))
	// doing ...
	s3.Failure(sentry.SpanStatusResourceExhausted)
	tx.Stop()

	// final result
	tx.Apply(
		sry.SpanLevel(sentry.LevelError),
		sry.SpanExtra("shots", 2),
		sry.SpanContextData("profile", map[string]interface{}{
			"flavor":  "vanilla",
			"content": []string{"ice_cream", "espresso"},
		}),
		sry.SpanScopeSet(func(s *sentry.Scope) {
			s.SetTag("progress", "blocked")
		}),
	)
	tx.Failure(sentry.SpanStatusInternalError)

	// Output:
}

// This function demonstrates how to use the sry package to record a message.
func Example_message() {
	_ = sry.InitSentryDebug("https://abc@123.ingest.sentry.io/456")
	defer sry.Flush()

	sry.SendMessage(
		context.Background(),
		"made a cup of joe",
		sry.ScopeLevel(sentry.LevelDebug),
		sry.ScopeTag("drink", "coffee"),
		sry.ScopeExtra("foo", "bar"),
		sry.ScopeContextData("profile", map[string]interface{}{
			"flavor":  "vanilla",
			"content": []string{"ice_cream", "espresso"},
		}),
	)

	// Output:
}

// This function demonstrates how to use the sry package to record an error.
func Example_exception() {
	_ = sry.InitSentryDebug("https://abc@123.ingest.sentry.io/456")
	defer sry.Flush()

	sry.SendException(
		context.Background(),
		fmt.Errorf("failed to make a cup of joe: %w", errors.New("no coffee beans")),
		sry.ScopeLevel(sentry.LevelFatal),
		sry.ScopeUser(sentry.User{IPAddress: "{{auto}}"}),
		sry.ScopeTag("drink", "coffee"),
		sry.ScopeContextData("profile", map[string]interface{}{
			"flavor":  "vanilla",
			"content": []string{"espresso"},
		}),
	)

	// Output:
}

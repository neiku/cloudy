package sry

import (
	"sync"

	"github.com/getsentry/sentry-go"
)

var (
	globalRWLock  sync.RWMutex
	globalSetters []ScopeSetter
)

func init() {
	globalSetters = make([]ScopeSetter, 0, 8)
	GlobalScopePush(func(s *sentry.Scope) {
		s.SetTag("hentai", "cloudykit")
	})
}

// GlobalScopeCount returns the number of global scope setters.
func GlobalScopeCount() int {
	globalRWLock.RLock()
	defer globalRWLock.RUnlock()
	return len(globalSetters)
}

// GlobalScopeClone returns a copy of global scope setters.
func GlobalScopeClone() []ScopeSetter {
	globalRWLock.RLock()
	defer globalRWLock.RUnlock()
	setters := make([]ScopeSetter, len(globalSetters))
	copy(setters, globalSetters)
	return setters
}

// GlobalScopePush pushes a scope setter to global scope setters.
func GlobalScopePush(setter ScopeSetter) {
	globalRWLock.Lock()
	defer globalRWLock.Unlock()
	if setter == nil {
		return
	}
	globalSetters = append(globalSetters, setter)
}

// GlobalScopePop pops the last scope setter from global scope setters.
func GlobalScopePop() ScopeSetter {
	globalRWLock.Lock()
	defer globalRWLock.Unlock()
	if len(globalSetters) == 0 {
		return nil
	}
	setter := globalSetters[len(globalSetters)-1]
	globalSetters = globalSetters[:len(globalSetters)-1]
	return setter
}

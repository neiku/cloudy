// Package sry provides a convenient way to send events and start transactions for Sentry.
package sry

import (
	"encoding/hex"
	"time"

	"github.com/getsentry/sentry-go"
)

// InitSentry initializes sentry-go with given DSN.
func InitSentry(dsn string) error {
	return sentry.Init(baseClientOptions(dsn))
}

// InitSentryDebug initializes sentry-go with given DSN and debug mode.
func InitSentryDebug(dsn string) error {
	opt := baseClientOptions(dsn)
	opt.Debug = true
	return sentry.Init(opt)
}

// InitSentryRelease initializes sentry-go with given release name.
func InitSentryRelease(dsn, release string) error {
	opt := baseClientOptions(dsn)
	opt.Release = release
	return sentry.Init(opt)
}

func baseClientOptions(dsn string) sentry.ClientOptions {
	return sentry.ClientOptions{
		Dsn:              dsn,
		TracesSampleRate: 1.0,
		AttachStacktrace: true,
	}
}

var (
	// FlushTimeout is the timeout for flushing events and transactions to Sentry.
	FlushTimeout = 5 * time.Second
)

// Flush flushes all events and transactions to Sentry.
func Flush() {
	sentry.Flush(FlushTimeout)
}

// NewTraceID creates a new TraceID from a string, it attempts to convert as Hex string first, and then ASCII.
func NewTraceID(s string) sentry.TraceID {
	t := sentry.TraceID{}
	var b []byte
	if h, err := hex.DecodeString(s); err == nil {
		b = h
	} else {
		b = []byte(s)
	}
	if len(b) > 16 {
		b = b[:16]
	}
	copy(t[:], b)
	return t
}

// NewSpanID creates a new SpanID from a string, it attempts to convert as Hex string first, and then ASCII.
func NewSpanID(s string) sentry.SpanID {
	t := sentry.SpanID{}
	var b []byte
	if h, err := hex.DecodeString(s); err == nil {
		b = h
	} else {
		b = []byte(s)
	}
	if len(b) > 8 {
		b = b[:8]
	}
	copy(t[:], b)
	return t
}

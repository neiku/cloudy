package sry

import (
	"testing"
)

func TestNewTraceID(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{"empty", "", "00000000000000000000000000000000"},
		{"single char", "a", "61000000000000000000000000000000"},
		{"single hex", "4a", "4a000000000000000000000000000000"},
		{"word", "aloha", "616c6f68610000000000000000000000"},
		{"half 64b", "203fea1b558b4a73", "203fea1b558b4a730000000000000000"},
		{"full 128b", "644e00bf6e79e4b88a5947e320f2d386", "644e00bf6e79e4b88a5947e320f2d386"},
		{"over 136b", "644e00bf6e79e4b88a5947e320f2d386ff", "644e00bf6e79e4b88a5947e320f2d386"},
		{"over 256b", "644e00bf6e79e4b88a5947e320f2d38617e05a34e8174844930e5742dc76adbc", "644e00bf6e79e4b88a5947e320f2d386"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewTraceID(tt.s); tt.want != got.String() {
				t.Errorf("NewTraceID() = %v, want %v", got.String(), tt.want)
			}
		})
	}
}

func TestNewSpanID(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{"empty", "", "0000000000000000"},
		{"single char", "a", "6100000000000000"},
		{"single hex", "4a", "4a00000000000000"},
		{"word", "aloha", "616c6f6861000000"},
		{"half 32b", "203fea1b", "203fea1b00000000"},
		{"full 64b", "203fea1b558b4a73", "203fea1b558b4a73"},
		{"over 256b", "644e00bf6e79e4b88a5947e320f2d38617e05a34e8174844930e5742dc76adbc", "644e00bf6e79e4b8"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewSpanID(tt.s); tt.want != got.String() {
				t.Errorf("NewSpanID() = %v, want %v", got.String(), tt.want)
			}
		})
	}
}
